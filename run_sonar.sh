#!/bin/sh

PROJECT=$(pwd)
TOKEN="1322809bd7a2336a7f59dd73c3d720a9ac425543" 

go test ./... -p 1 -count 1 -coverprofile=coverage.out
go test ./... -p 1 -count 1 -json > report.json

# TODO validate/test
#docker run --rm --network host -e SONAR_HOST_URL="http://127.0.0.1:9000" -e SONAR_LOGIN=$TOKEN -v "$PROJECT:/usr/src"  sonarsource/sonar-scanner-cli -Dproject.settings=sonar-project.properties

