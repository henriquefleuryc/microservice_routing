# Sonarqube static code analysis

## Requirements
 - Running Sonarqube server acessible by host/builder/CI environment IP or hostname (to be added as `SONAR_HOST_URL` on run_sonar.[bat|sh] script)
 - Valid Sonarqube token (to be added to run_sonar.[bat|sh] script) 

## How-to
 - Update run_sonar script based on current platform (use `bat` for Windows even when using GitBash, Cygwin, MinGW. Validade the `sh` script and change accordingly)
 - run the script
 - Done, check your Sonarqube server project´s page