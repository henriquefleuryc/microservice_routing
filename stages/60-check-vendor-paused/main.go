package main

import (
	"context"
	"os"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
)

var log *logger.Log
var client graphclient.FHGraphql

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}
	client = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)

}

func handler(ctx context.Context, input model.Order) (order model.Order, err error) {
	salesChannelOrderID := int64(order.SalesChannelOrderID)
	log, err = logger.NewOrderLog(&input.ID, &salesChannelOrderID, (*string)(&input.SalesChannel), (*string)(&order.Brand))
	if err != nil {
		return model.Order{}, err
	}
	log.Logger.Info("60-check-vendor-paused - start")

	order = input

	for _, pkg := range order.Packages {
		vendor, err := client.GetVendor(pkg.VendorID)
		if err != nil {
			log.Logger.Printf("GetVendor error %+v VendorID: %d PackageID: %d\n", err.Error(), pkg.VendorID, pkg.ID)
			continue
		}

		if vendor.Paused {
			log.Logger.Printf("Changing Package Status to VENDOR_PAUSED VendorID: %d PackageID: %d\n", pkg.VendorID, pkg.ID)
			pkg.Status = model.PackageStatusVendorPaused
			err = client.UpdatePackage(pkg)
			if err != nil {
				log.Logger.Printf("Error %+v VendorID: %d PackageID: %d\n", err.Error(), pkg.VendorID, pkg.ID)
				continue
			}
		}
	}

	return order, nil
}

func main() {
	lambda.Start(handler)
}
