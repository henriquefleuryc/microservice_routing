package main

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/client/fh-graphql/graph/model"
	"github.com/joho/godotenv"
	"github.com/shurcooL/graphql"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMain(m *testing.M) {
	godotenv.Load("./../../.env")
	sess = session.Must(session.NewSession(&aws.Config{
		Endpoint: aws.String("http://localhost:8000"),
	}))
	dynSvc = dynamodb.New(sess)
	tableName = "FHRoutingCarrierDistribution"

	m.Run()
}

func TestCarrierShipment(t *testing.T) {
	cs := carrierShipment{
		Day:        today,
		Carrier:    "FEDEX",
		Proportion: 50,
		Total:      0,
	}

	resetTestTable()

	t.Run("create", func(t *testing.T) {
		err := cs.create()
		require.Nil(t, err)

		shipments, err := loadShipments()
		require.Nil(t, err)
		require.True(t, len(shipments) > 0)
	})

	t.Run("update", func(t *testing.T) {
		cs.Total++
		err := cs.update()
		require.Nil(t, err)

		shipments, err := loadShipments()
		require.Nil(t, err)
		require.True(t, len(shipments) > 0)
		// require.Equal(t, 1, shipments[0].Total)
	})
}

func TestIsPOBox(t *testing.T) {
	cases := []struct {
		Value string
		Want  bool
	}{
		{"742 Evergreen Terrace", false},
		{"742 Evergreen Terrace P.O. Box 123", true},
		{"post office # 100", true},
		{"box 123", true},
		{"500 Main Street", false},
		{"PO BOX #534", true},
		{"PO BOX # 534", true},
		{"PO BOX    #    534", true},
		{"PO BOX # ", false},
		{"PO BOX   #    ", false},
		{"PO BOX     ", false},
	}

	for _, tt := range cases {
		require.Equal(
			t,
			tt.Want,
			isPOBox(&model.Order{
				Street: tt.Value,
			}),
			fmt.Sprintf("Expected %v for %s", tt.Want, tt.Value),
		)
	}
}

func TestChooseBetweenCarriers(t *testing.T) {
	t.Run("even distribution", func(t *testing.T) {
		carriers = []model.ShippingCarrier{
			{
				ID:         1,
				Name:       "A",
				Proportion: 50,
			},
			{
				ID:         2,
				Name:       "B",
				Proportion: 50,
			},
		}

		resetTestTable()
		shipments, _ = loadShipments()

		expectations := []string{
			"A", // 1/0 with zero shipments take the biggest proportion
			"B", // 1/1 proportion matched
			"A", // 2/1 actual proportion was right so use first match
			"B", // 2/2 proportion matched
			"A", // 3/2 actual proportion was right so use first match
			"B", // 3/3 proportion matched
			"A", // 4/3 actual proportion was right so use first match
			"B", // 4/4 proportion matched
		}

		for i, id := range expectations {
			require.Equal(t, id, chooseBetweenCarriers("A", "B", "C"), fmt.Sprintf("Failed at %d", i))
		}
	})

	t.Run("uneven distribution", func(t *testing.T) {
		carriers = []model.ShippingCarrier{
			{
				ID:         1,
				Name:       "A",
				Proportion: 50,
			},
			{
				ID:         2,
				Name:       "B",
				Proportion: 25,
			},
			{
				ID:         3,
				Name:       "C",
				Proportion: 25,
			},
		}

		resetTestTable()
		shipments, _ = loadShipments()

		expectations := []string{
			"A", // 1/0/0 with zero shipments take the biggest proportion
			"B", // 1/1/0 as A is ahead and its the last one having the same proportion as C
			"C", // 1/1/1 as its the one with zero shipments
			"A", // 2/1/1 proportion matched
			"A", // 3/1/1 actual proportion was right so use first match
			"B", // 3/2/1
			"C", // 3/2/2
			"A", // 4/2/2 proportion matched
			"A", // 5/2/2 actual proportion was right so use first match
			"B", // 5/3/2
			"C", // 5/3/3
			"A", // 6/3/3 proportion matched
			"A", // 7/3/3 actual proportion was right so use first match
			"B", // 7/4/3
			"C", // 7/4/4
			"A", // 8/4/4 proportion matched
		}

		for i, id := range expectations {
			require.Equal(t, id, chooseBetweenCarriers("A", "B", "C"), fmt.Sprintf("Failed at %d", i))
		}
	})

	t.Run("uneven distribution zero proportion contender", func(t *testing.T) {
		carriers = []model.ShippingCarrier{
			{
				ID:         1,
				Name:       "A",
				Proportion: 50,
			},
			{
				ID:         2,
				Name:       "B",
				Proportion: 0,
			},
		}

		resetTestTable()
		shipments, _ = loadShipments()
		expectations := []string{
			"A",
			"A",
			"A",
			"A",
		}

		for i, id := range expectations {
			log.Logger.Println(" Expectation: ", i, id)
			require.Equal(t, id, chooseBetweenCarriers("A", "B", "C"), fmt.Sprintf("Failed at %d", i))
		}
	})

}

func TestSelectCarrier(t *testing.T) {
	carriers = []model.ShippingCarrier{
		{
			ID:         1,
			Name:       "FEDEX",
			Proportion: 33,
		},
		{
			ID:         2,
			Name:       "UPS",
			Proportion: 33,
		},
		{
			ID:         3,
			Name:       "USPS",
			Proportion: 34,
		},
	}

	for _, carrier := range carriers {
		shipments = append(shipments, carrierShipment{
			Day:        today,
			Carrier:    carrier.Name,
			Proportion: carrier.Proportion,
			Total:      0,
		})
	}

	cases := []struct {
		Street       string
		Country      string
		ItemsWeigths []float64
		WantWeight   float64
		WantCarrier  string
		WantMethod   string
	}{
		// Canada
		{"PO BOX #345", "CA", []float64{0.8}, 0.8, "FEDEX", "International"},

		// Bellow 0.95 pound with or without PO BOX
		{"My Street #345", "US", []float64{0.8}, 0.8, "USPS", "First Class"},
		{"PO BOX #345", "US", []float64{0.8}, 0.8, "USPS", "First Class"},
		{"PO BOX 345", "US", []float64{0.1, 0.4}, 0.5, "USPS", "First Class"},

		// PO BOX, equal to or above 0.95 pound
		{"PO BOX #345", "US", []float64{0.01, 0.94}, 0.95, "FEDEX", "SmartPost"},
		{"PO BOX #345", "US", []float64{0.01, 0.95}, 0.96, "FEDEX", "SmartPost"},

		// Above 8.5 pounds no PO BOX
		{"New Street #345", "US", []float64{9.0, 0.1}, 9.1, "FEDEX", "Ground"},
		{"New Street #345", "US", []float64{9.0, 0.1}, 9.1, "UPS", "Ground"},
		{"New Street #345", "US", []float64{9.0, 0.1}, 9.1, "FEDEX", "Ground"},

		// Between 0.95 and 8 no PO BOX, no signature not for Canada
		{"New Street #345", "US", []float64{8.0, 0.1}, 8.1, "FEDEX", "SmartPost"},
	}

	for i, tt := range cases {
		o := &model.Order{
			Street:  tt.Street,
			Country: tt.Country,
		}

		p := &model.Package{}

		for ii, weight := range tt.ItemsWeigths {
			p.Items = append(p.Items, &model.Item{
				Weight:       weight,
				Quantity:     1,
				ProductPid:   ii,
				ProductBrand: "PAT",
			})
		}

		err := selectCarrier(o, p)
		require.Nil(t, err, fmt.Sprintf("Failed at %d", i))
		require.Equal(t, tt.WantWeight, p.Weight, fmt.Sprintf("Failed at %d", i))
		require.Equal(t, tt.WantCarrier, p.ShippingCarrier, fmt.Sprintf("Failed at %d", i))
		require.Equal(t, tt.WantMethod, p.ShippingMethod, fmt.Sprintf("Failed at %d", i))
	}

}

func createTestTable() error {
	input := &dynamodb.CreateTableInput{
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("Day"),
				AttributeType: aws.String("S"),
			},
			{
				AttributeName: aws.String("Carrier"),
				AttributeType: aws.String("S"),
			},
		},
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("Day"),
				KeyType:       aws.String("HASH"),
			},
			{
				AttributeName: aws.String("Carrier"),
				KeyType:       aws.String("RANGE"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{
			ReadCapacityUnits:  aws.Int64(5),
			WriteCapacityUnits: aws.Int64(5),
		},
		TableName: aws.String(tableName),
	}

	if _, err := dynSvc.CreateTable(input); err != nil {
		log.Logger.Print(err)
		return err
	}

	return nil
}

func deleteTestTable() {
	dynSvc.DeleteTable(&dynamodb.DeleteTableInput{
		TableName: aws.String(tableName),
	})
}

func resetTestTable() {
	deleteTestTable()
	createTestTable()
}

func TestCheckForShippingExclussions(t *testing.T) {
	// Arrange
	assert := assert.New(t)
	//pimclient = graphql.NewClient("https://pim-fh.sb.client.net/query", nil)
	pimclient = mockPimClientWithResponse(`{
		"data": {
			"products": [
			{
				"shipping": {
				"exclusions": [
					"Canada",
					"militaryPoBox",
					"poBox",
					"HI",
					"AK"
				]
				}
			}
			]
		}
	}`)
	testCases := []struct {
		name        string
		input       *model.Order
		expected    bool
		exstatus    model.PackageStatus
		shoulderror bool
	}{
		{name: "Order with no excluded items should process",
			input: &model.Order{
				Country: "US",
				Street:  "181 Glen Creek St. Miami",
				State:   "FL",
				Packages: []*model.Package{
					{
						Items: []*model.Item{
							{ProductPid: 302, ProductBrand: "PAT"},
						},
						Status: "CREATED",
					},
				},
				Items: []*model.Item{
					{ProductPid: 302, ProductBrand: "PAT"},
				},
			},
			expected: false, exstatus: model.PackageStatusCreated, shoulderror: false},
		{name: "Order with excluded state should return true",
			input: &model.Order{
				Country: "US",
				Street:  "181 Glen Creek St. Miami",
				State:   "HI",
				Packages: []*model.Package{
					{
						Items: []*model.Item{
							{ProductPid: 302, ProductBrand: "PAT"},
						},
						Status: "CREATED",
					},
				},
				Items: []*model.Item{
					{ProductPid: 302, ProductBrand: "PAT"},
				},
			},
			expected: true, exstatus: model.PackageStatusNoship, shoulderror: false},
		{name: "Order with excluded country should return true",
			input: &model.Order{
				Country: "CA",
				Street:  "181 Glen Creek St. Miami",
				State:   "FL",
				Packages: []*model.Package{
					{
						Items: []*model.Item{
							{ProductPid: 302, ProductBrand: "PAT"},
						},
						Status: "CREATED",
					},
				},
				Items: []*model.Item{
					{ProductPid: 302, ProductBrand: "PAT"},
				},
			},
			expected: true, exstatus: model.PackageStatusNoship, shoulderror: false},
		{name: "Order with pobox address should return true",
			input: &model.Order{
				Country: "US",
				Street:  "PO Box 1054, Miami",
				State:   "FL",
				Packages: []*model.Package{
					{
						Items: []*model.Item{
							{ProductPid: 302, ProductBrand: "PAT"},
						},
						Status: "CREATED",
					},
				},
				Items: []*model.Item{
					{ProductPid: 302, ProductBrand: "PAT"},
				},
			},
			expected: true, exstatus: model.PackageStatusNoship, shoulderror: false},
		{name: "Order with military pobox address should return true",
			input: &model.Order{
				Country: "IQ",
				Street:  "UNIT 2340 APO BOX 132 AE 09350",
				State:   "",
				Packages: []*model.Package{
					{
						Items: []*model.Item{
							{ProductPid: 302, ProductBrand: "PAT"},
						},
						Status: "CREATED",
					},
				},
				Items: []*model.Item{
					{ProductPid: 302, ProductBrand: "PAT"},
				},
			},
			expected: true, exstatus: model.PackageStatusNoship, shoulderror: false},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			// Act
			res, err := checkForShippingExclusions(tc.input)

			// Assert
			assert.NoError(err)
			assert.Equal(res, tc.expected, "%v check failed, expected %v, got %v", tc.name, tc.expected, res)
			assert.Equal(tc.input.Packages[0].Status, tc.exstatus, "%v check failed, expected %v, got %v", tc.name, tc.exstatus, tc.input.Packages[0].Status)
		})
	}

}

// TestSelectCarrierShouldNotDuplicateWeight runs a test for bug https://jira4patriot.atlassian.net/browse/FH-791
func TestSelectCarrierShouldNotDuplicateWeight(t *testing.T) {
	// Arrange
	assert := assert.New(t)
	pckgID := 19
	is65Prop := true
	inputorder := &model.Order{
		ID:       114,
		ParentID: nil,
		Brand:    "PAT",
		Packages: []*model.Package{
			{ID: 19, OrderID: 114, SignatureRequired: false, Weight: 15.9, Status: "CREATED", Is65prod: &is65Prop,
				Items: []*model.Item{
					{ID: 172, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 15.9},
					{ID: 173, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 0},
					{ID: 174, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 0},
				},
			},
		},
	}
	inputpackage := &model.Package{ID: 19, OrderID: 114, SignatureRequired: false, Weight: 15.9, Status: "CREATED", Is65prod: &is65Prop,
		Items: []*model.Item{
			{ID: 172, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 15.9},
			{ID: 173, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 0},
			{ID: 174, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 0},
		},
	}
	cs := carrierShipment{
		Day:        today,
		Carrier:    "FEDEX",
		Proportion: 50,
		Total:      0,
	}

	resetTestTable()
	err := cs.create()
	carriers = []model.ShippingCarrier{
		{
			ID:         1,
			Name:       "FEDEX",
			Proportion: 50,
		},
		{
			ID:         2,
			Name:       "USPS",
			Proportion: 50,
		},
	}
	shipments, err = loadShipments()

	// Act
	err = selectCarrier(inputorder, inputpackage)

	// Assert
	assert.NoError(err)
	assert.Equal(15.9, inputpackage.Weight)
}

// TestSelectCarrierShouldReturnCorrectFEDEXCode runs a test for bug https://jira4patriot.atlassian.net/browse/FH-800
func TestSelectCarrierShouldReturnCorrectFEDEXCode(t *testing.T) {
	// Arrange
	assert := assert.New(t)
	pckgID := 19
	is65Prop := true
	inputorder := &model.Order{
		ID:       114,
		ParentID: nil,
		Brand:    "PAT",
		Packages: []*model.Package{
			{ID: 19, OrderID: 114, SignatureRequired: false, Weight: 15.9, Status: "CREATED", Is65prod: &is65Prop,
				Items: []*model.Item{
					{ID: 172, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 15.9},
					{ID: 173, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 0},
					{ID: 174, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 0},
				},
			},
		},
	}
	inputpackage := &model.Package{ID: 19, OrderID: 114, SignatureRequired: false, Weight: 15.9, Status: "CREATED", Is65prod: &is65Prop,
		Items: []*model.Item{
			{ID: 172, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 15.9},
			{ID: 173, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 0},
			{ID: 174, OrderID: 114, PackageID: &pckgID, SignatureRequired: false, Quantity: 1, Weight: 0},
		},
	}
	cs := carrierShipment{
		Day:        today,
		Carrier:    "FEDEX",
		Proportion: 50,
		Total:      0,
	}

	resetTestTable()
	err := cs.create()
	carriers = []model.ShippingCarrier{
		{
			ID:         1,
			Name:       "FEDEX",
			Proportion: 50,
		},
		{
			ID:         2,
			Name:       "USPS",
			Proportion: 50,
		},
	}
	shipments, err = loadShipments()

	// Act
	err = selectCarrier(inputorder, inputpackage)

	// Assert
	assert.NoError(err)
	assert.Equal("FGNDRTB", inputpackage.ShippingCode)
}

// Helpers and Mockers
func mockPimClientWithResponse(response string) *graphql.Client {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, response)
	})

	return graphql.NewClient("/query", &http.Client{Transport: localRoundTripper{handler: mux}})
}

func mustWrite(w io.Writer, s string) {
	_, err := io.WriteString(w, s)
	if err != nil {
		panic(err)
	}
}

// localRoundTripper is an http.RoundTripper that executes HTTP transactions
// by using handler directly, instead of going over an HTTP connection.
type localRoundTripper struct {
	handler http.Handler
}

func (l localRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	w := httptest.NewRecorder()
	l.handler.ServeHTTP(w, req)
	return w.Result(), nil
}

func TestSelectCarrierZeroProportionContender(t *testing.T) {

	carriers = []model.ShippingCarrier{
		{
			ID:         1,
			Name:       "FEDEX",
			Proportion: 0,
		},
		{
			ID:         2,
			Name:       "UPS",
			Proportion: 50,
		},
		{
			ID:         3,
			Name:       "USPS",
			Proportion: 50,
		},
	}

	resetTestTable()

	for _, carrier := range carriers {
		shipment := carrierShipment{
			Day:        today,
			Carrier:    carrier.Name,
			Proportion: carrier.Proportion,
		}
		switch carrier.Name {
		case "FEDEX":
			shipment.Total = 3
		case "UPS":
			shipment.Total = 4
		default:
			shipment.Total = 0
		}

		shipment.create()
	}

	shipments, _ = loadShipments()

	cases := []struct {
		Street       string
		Country      string
		ItemsWeigths []float64
		WantWeight   float64
		WantCarrier  string
		WantMethod   string
	}{
		// Above equals 0.95 pounds, not for Canada, no signature, PO Box,  proportion always FEDEX
		{"PO BOX #345", "US", []float64{68.0, 0.2}, 68.2, "FEDEX", "SmartPost"},
		{"PO BOX #345", "US", []float64{0.0, 0.95}, 0.95, "FEDEX", "SmartPost"},
		// Below 0.95 pounds, not for Canada, no signature,  with or without PO Box, USPS
		{"PO BOX #345", "US", []float64{0.0, 0.94}, 0.94, "USPS", "First Class"},
		{"My Street #123", "US", []float64{0.0, 0.5}, 0.5, "USPS", "First Class"},
		// Above 8.5, not for Canada, no PO box, no signature UPS since FEDEX
		{"My Street #223", "US", []float64{8.0, 0.6}, 8.6, "UPS", "Ground"},
	}

	for i, tt := range cases {
		o := &model.Order{
			Street:  tt.Street,
			Country: tt.Country,
		}

		p := &model.Package{}

		for ii, weight := range tt.ItemsWeigths {
			p.Items = append(p.Items, &model.Item{
				Weight:       weight,
				Quantity:     1,
				ProductPid:   ii,
				ProductBrand: "PAT",
			})
		}

		err := selectCarrier(o, p)
		require.Nil(t, err, fmt.Sprintf("Failed at %d", i))
		require.Equal(t, tt.WantWeight, p.Weight, fmt.Sprintf("Failed at %d", i))
		require.Equal(t, tt.WantCarrier, p.ShippingCarrier, fmt.Sprintf("Failed at %d", i))
		require.Equal(t, tt.WantMethod, p.ShippingMethod, fmt.Sprintf("Failed at %d", i))
	}

}
