package main

import (
	"context"
	"errors"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
	"github.com/shurcooL/graphql"
)

var (
	sess      *session.Session          = session.Must(session.NewSession())
	dynSvc    dynamodbiface.DynamoDBAPI = dynamodb.New(sess)
	today     string                    = time.Now().Format("2006-01-02")
	tableName string                    = os.Getenv("CARRIER_DISTRIBUTION_TABLE")
	client    graphclient.FHGraphql
	pimclient *graphql.Client
	shipments []carrierShipment
	carriers  []model.ShippingCarrier
	products  []model.Product
	log       *logger.Log
)

type carrierShipment struct {
	Day        string
	Carrier    string
	Proportion int
	Total      int
}

// Product Type used to query for product exclusions on PIM
type Product struct {
	Shipping *Shipping `graphql:"shipping"  json:"shipping"`
}

// Shipping ...
type Shipping struct {
	Exclusions []*string `graphql:"exclusions" json:"exclusions"`
}

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}

	client = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)
	pimclient = graphql.NewClient(os.Getenv("PIM_SERVICE_URL"), httpClient)

}

func (shipment *carrierShipment) create() (err error) {
	item, err := dynamodbattribute.MarshalMap(shipment)
	if err != nil {
		return
	}

	input := &dynamodb.PutItemInput{
		Item:      item,
		TableName: aws.String(tableName),
	}

	_, err = dynSvc.PutItem(input)

	return
}

func (shipment *carrierShipment) update() (err error) {
	input := &dynamodb.UpdateItemInput{
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":Proportion": {
				N: aws.String(strconv.Itoa(shipment.Proportion)),
			},
			":Total": {
				N: aws.String(strconv.Itoa(shipment.Total)),
			},
		},
		TableName: aws.String(tableName),
		Key: map[string]*dynamodb.AttributeValue{
			"Day": {
				S: &today,
			},
			"Carrier": {
				S: &shipment.Carrier,
			},
		},
		ReturnValues:             aws.String("UPDATED_NEW"),
		UpdateExpression:         aws.String("SET Proportion = :Proportion, #Total = :Total"),
		ExpressionAttributeNames: map[string]*string{"#Total": aws.String("Total")},
	}

	_, err = dynSvc.UpdateItem(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			log.Logger.Print(aerr.Error())
		}
		return
	}

	return
}

func (shipment *carrierShipment) add() (err error) {
	shipment.Total++
	return shipment.update()
}

func loadShipments() (shipments []carrierShipment, err error) {
	input := &dynamodb.QueryInput{
		TableName: aws.String(tableName),
		ExpressionAttributeValues: map[string]*dynamodb.AttributeValue{
			":Day": {
				S: &today,
			},
		},
		KeyConditionExpression:   aws.String("#Day = :Day"),
		ExpressionAttributeNames: map[string]*string{"#Day": aws.String("Day")},
		ConsistentRead:           aws.Bool(true),
	}

	result, err := dynSvc.Query(input)
	if err != nil {
		return
	}

	for _, item := range result.Items {
		var cd carrierShipment

		err = dynamodbattribute.UnmarshalMap(item, &cd)
		if err != nil {
			return
		}

		shipments = append(shipments, cd)
	}

	for i := range carriers {
		var found bool
		for ii := range shipments {
			if carriers[i].Name == shipments[ii].Carrier {
				if carriers[i].Proportion != shipments[ii].Proportion {
					shipments[ii].Proportion = carriers[i].Proportion
					err = shipments[ii].update()
					if err != nil {
						return
					}
				}
				found = true
			}
		}

		if !found {
			cs := carrierShipment{
				Day:        today,
				Carrier:    carriers[i].Name,
				Proportion: carriers[i].Proportion,
				Total:      0,
			}

			err = cs.create()
			if err != nil {
				return
			}

			shipments = append(shipments, cs)
		}
	}

	return
}

func isSignatureRequired(item *model.Item) (sr bool) {
	return item.SignatureRequired
}

func isShippingToCanada(order *model.Order) bool {
	return order.Country == "CA"
}

func isPOBox(order *model.Order) bool {
	r, err := regexp.Compile(`^(((p[\s.\-/]?[o\s][.-]?)\s?)|(post\s?(office\s)?))((box|bin|b\.?)?\s?(num|number|#)?\s?\d+)`)
	if err != nil {
		panic(err)
	}
	if r.MatchString(order.Street) {
		return true
	}
	r, err = regexp.Compile(`\b(P|p)(ost|ostal)?([ \.]*(O|0|o)(ffice)?)?([ \.]*(b|B))?(ox|OX)?([ ]+)?(num|number|#)?([ ]+)?[0-9]`)
	if err != nil {
		panic(err)
	}
	if r.MatchString(order.Street) {
		return true
	}

	r, err = regexp.Compile(`(b|B)+(ox|OX)+([ ]+)?(num|number|#)?([ ]+)?([0-9])+`)
	if err != nil {
		panic(err)
	}
	if r.MatchString(order.Street) {
		return true
	}

	return false
}

func chooseBetweenCarriers(carriersNames ...string) (selectedCarrierName string) {
	var totalShipments int
	var totalProportion int
	var contenders []*carrierShipment
	for i, shipment := range shipments {
		for _, carrierName := range carriersNames {
			if carrierName == shipment.Carrier {
				contenders = append(contenders, &shipments[i])
				totalShipments += shipment.Total
				totalProportion += shipment.Proportion
			}
		}
	}

	var selected *carrierShipment

	if totalShipments == 0 {
		var biggestProportion int
		for i := range contenders {
			if biggestProportion < contenders[i].Proportion {
				biggestProportion = contenders[i].Proportion
				selected = contenders[i]
			}
		}
		selected.add()

		return selected.Carrier
	}

	for _, contender := range contenders {
		actualProportion := (float64(contender.Total) / float64(totalShipments)) * 100
		targetProportion := (float64(contender.Proportion) / float64(totalProportion)) * 100
		if actualProportion < targetProportion {
			selected = contender
			break
		}
	}

	if selected == nil {
		var biggestProportion int
		for i := range contenders {
			if biggestProportion < contenders[i].Proportion {
				biggestProportion = contenders[i].Proportion
				selected = contenders[i]
				break
			}
		}
	}

	selected.add()
	return selected.Carrier
}

func selectCarrier(order *model.Order, pkg *model.Package) (err error) {
	var hasSignatureRequiredItem bool

	pkg.Weight = 0
	for _, item := range pkg.Items {
		pkg.Weight = pkg.Weight + (item.Weight * float64(item.Quantity))

		if isSignatureRequired(item) {
			hasSignatureRequiredItem = true
		}
	}

	if isShippingToCanada(order) {
		code := "FINTLETB"
		if hasSignatureRequiredItem {
			code = "FINTLETBSIG"
		}
		setShippingCarrierByCode(code, pkg)
		return
	}

	if hasSignatureRequiredItem {
		pkg.ShippingCarrier = chooseBetweenCarriers("FEDEX", "UPS")
		code := "UPSRTBSR"
		if pkg.ShippingCarrier == "FEDEX" {
			code = "FGNDRTBSR"
		}

		setShippingCarrierByCode(code, pkg)

		return
	}

	if isPOBox(order) {
		code := "USFCSP" // pkg.Weight < 0.95 exclusive "US FC"
		if pkg.Weight >= 0.95 {
			code = "FSPTPB"
		}

		setShippingCarrierByCode(code, pkg)
		return
	}

	if pkg.Weight >= 8.5 {
		pkg.ShippingCarrier = chooseBetweenCarriers("FEDEX", "UPS")
		code := "UPSRTB"
		if pkg.ShippingCarrier == "FEDEX" {
			code = "FGNDRTB"
		}

		setShippingCarrierByCode(code, pkg)

		return
	}

	if (pkg.Weight >= 0.95) && (pkg.Weight < 8.5) {
		setShippingCarrierByCode("FSPTPB", pkg)
		return
	}

	// Default case weight < 0.95
	setShippingCarrierByCode("USFCSP", pkg)

	return
}

func checkForShippingExclusions(order *model.Order) (bool, error) {
	log.Logger.Println("Starting check for shipping exclusions, packages = ", len((*order).Packages))
	for _, pkg := range order.Packages {
		if pkg.ShippingCode != "" {
			continue
		}
		expackage, err := checkPackageForExclusions(order, pkg)
		log.Logger.Printf("Exclusion check result = %+v", expackage)
		if err != nil {
			return false, err
		}
		if expackage {
			log.Logger.Println("Exclusions found on order")
			return true, nil
		}
	}
	log.Logger.Println("No exclusions found for order")
	return false, nil
}

func checkPackageForExclusions(order *model.Order, pkg *model.Package) (bool, error) {
	log.Logger.Println("Starting check for package, items = ", len((*pkg).Items))
	for _, item := range (*pkg).Items {
		log.Logger.Printf("Loading exclusions for item %+v, %+v", item.ProductPid, item.ProductBrand)
		pe, err := queryPimExclusion(item.ProductPid, item.ProductBrand)
		if err != nil {
			log.Logger.Printf("Error querying PIM : %+v", err)
			return false, err
		}
		log.Logger.Printf("Checking exclusions for item %+v with exclusions : %+v", *item, *pe)
		if checkForItemExclusion(*order, *pe) {
			log.Logger.Printf("Exclusion found for %+v, with exclusions : %+v", item, pe)
			pkg.Status = model.PackageStatusNoship
			return true, nil
		}
	}
	return false, nil
}

func checkForItemExclusion(order model.Order, productexclusions []Product) bool {
	for _, p := range productexclusions {
		for _, e := range p.Shipping.Exclusions {
			log.Logger.Printf("Validating exclusion %v for data Country: %s, State: %s, Street: %s", e, order.Country, order.State, order.Street)
			switch strings.ToLower(*e) {
			case "militarypobox":
				if checkIfAddressIsMilitaryPoBox(order) {
					return true
				}
			case "pobox":
				if isPOBox(&order) {
					return true
				}
			case "canada":
				if order.Country == "CA" {
					return true
				}
			default:
				if order.State == *e {
					return true
				}
			}
		}
	}
	return false
}

func checkIfAddressIsMilitaryPoBox(order model.Order) bool {
	if ok, _ := regexp.Match(`([af][ .]?p[ .]?o|[Pp]*(OST|ost)*)\b\.*\s*[Oo0]*(ffice|FFICE)*\.*\s*[Bb][Oo0][Xx]`, []byte(order.Street)); ok {
		return true
	}
	return false
}

func queryPimExclusion(productPid int, brand string) (*[]Product, error) {
	var query struct {
		Products []Product `graphql:"products(product_id: $product_id,brand: $brand)"`
	}

	brand = strings.ToLower(brand)
	variables := map[string]interface{}{
		"product_id": graphql.String(strconv.Itoa(productPid)),
		"brand":      graphql.String(brand),
	}

	if err := pimclient.Query(context.Background(), &query, variables); err != nil {
		return nil, err
	}
	return &query.Products, nil
}

func handler(ctx context.Context, order model.Order) (model.Order, error) {
	var e error
	salesChannelOrderID := int64(order.SalesChannelOrderID)
	log, e = logger.NewOrderLog(&order.ID, &salesChannelOrderID, (*string)(&order.SalesChannel), (*string)(&order.Brand))
	if e != nil {
		return model.Order{}, e
	}
	log.Logger.Info("40-select-carrier-65 - start")

	prodexcl, err := checkForShippingExclusions(&order)
	if err != nil {
		return order, err
	}
	if prodexcl {
		log.Logger.Printf("Products with exclusions found, %+v", order.Packages[0])
		if err := client.UpdateOrder(&order); err != nil {
			return order, err
		}
		return order, errors.New("Some items in the order cannot be shipped, please review the exclusions")
	}

	carriers, err = client.FindAllShippingCarriers()
	if err != nil {
		return order, err
	}

	shipments, err = loadShipments()
	if err != nil {
		return order, err
	}

	for i := range order.Packages {
		err = selectCarrier(&order, order.Packages[i])
		if err != nil {
			return order, err
		}
	}
	return order, nil
}

func main() {
	lambda.Start(handler)
}

func setShippingCarrierByCode(code string, pkg *model.Package) {

	shipmentInfo := map[string]struct {
		carrier string
		method  string
	}{
		"FGNDRTBSR":   {"FEDEX", "Ground"},
		"FGNDRTB":     {"FEDEX", "Ground"},
		"FSPTPB":      {"FEDEX", "SmartPost"},
		"FINTLETBSIG": {"FEDEX", "International SR"},
		"FINTLETB":    {"FEDEX", "International"},
		"UPSRTB":      {"UPS", "Ground"},
		"UPSRTBSR":    {"UPS", "Residential"},
		"USFCSP":      {"USPS", "First Class"},
	}

	if info, ok := shipmentInfo[code]; ok {
		pkg.ShippingCarrier = info.carrier
		pkg.ShippingMethod = info.method
		pkg.ShippingCode = code
	}
}
