package main

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/client/fh-graphql/graph/model"
	"github.com/joho/godotenv"
	"github.com/shurcooL/graphql"
	"github.com/stretchr/testify/assert"
)

func init() {
	godotenv.Load("./../../.env")
}

/*func TestHandler(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, `{
				"data":{
				   "products":[
					  {
						 "product_id":"18",
						 "brand":"pat",
						 "weight":25,
						 "fulfillments":{
							"fulfillment_id":"18"
						 },
						 "product_codes":[
							{
							   "upc":"810026140532",
							   "weight":25,
							   "quantity":1,
							   "signature_required":true
							}
						 ],
						 "shipping":{
							"exclusions":[
							   "HI"
							]
						 }
					  }
				   ]
				}
			 }`)
	})

	pimclient = graphql.NewClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}},
	)

	orderJSON := `{
		"id": 530,
		"parentId": null,
		"brand": "PAT",
		"items": [
		  {
			"id": 1159,
			"orderId": 530,
			"packageId": null,
			"productPid": 18,
			"productBrand": "PAT",
			"originalPid": 0,
			"signatureRequired": false,
			"quantity": 1,
			"weight": 0,
			"skus": null,
			"trackingNumber": null,
			"createdAt": "2021-02-24T22:35:52Z",
			"updatedAt": "2021-02-25T16:16:45Z"
		  }
		],
		"packages": null,
		"children": null,
		"firstname": "Stephanie",
		"lastname": "test",
		"email": "Stephanietest1614205404@test.com",
		"phone": "6154783972",
		"street": "noship2",
		"city": "Murfreesboro",
		"zip": "37128",
		"state": "TN",
		"country": "US",
		"salesChannel": "PORTAL",
		"salesChannelOrderId": 5531830,
		"status": "BUNDLED",
		"upgradedById": null,
		"upgradedAt": null,
		"paused": false,
		"pausedAt": null,
		"pauseReason": null,
		"resumedAt": null,
		"taskToken": null,
		"error": {
		  "Error": "errorString",
		  "Cause": "{\"errorMessage\":\"slice doesn't exist in any of 1 places to unmarshal\",\"errorType\":\"errorString\"}"
		},
		"fieldsUpdated": null,
		"createdAt": "2021-02-24T22:35:52Z",
		"updatedAt": "2021-02-25T16:16:45Z"
	  }`

	inputOrder := &model.Order{}
	json.Unmarshal([]byte(orderJSON), &inputOrder)

	o, err := handler(nil, *inputOrder)
	assert.Nil(t, err)
	assert.NotNil(t, o)
	assert.Equal(t, strings.ToUpper(string(o.Brand)), "PAT")
	assert.Equal(t, o.Items[0].ProductPid, 18)
	assert.Equal(t, strings.ToUpper(o.Items[0].ProductBrand), "PAT")
	assert.Equal(t, o.Items[0].OriginalPid, 18)
	assert.Equal(t, o.Items[0].SignatureRequired, true)
	assert.Equal(t, o.Items[0].Quantity, 1)
	assert.Equal(t, o.Items[0].Weight, 25.0)
	assert.Equal(t, o.Items[0].Skus[0].Codes[0], "810026140532")
	assert.Equal(t, o.Items[0].Skus[0].Quantity, 1)
	assert.Equal(t, strings.ToUpper(string(o.Status)), "BUNDLED")
	assert.Equal(t, strings.TrimSpace(*o.Error), "")
}
*/

func TestGetItemsShippingIDs(t *testing.T) {
	t.Run("Found", func(t *testing.T) {
		mux := http.NewServeMux()
		mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			mustWrite(w, `{
				"data": {
					"products": [
						{
							"product_id": "740",
							"brand": "pat",
							"product_codes": [
								{
									"upc": "810026140174",
									"weight": 0.8,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140488",
									"weight": 0.05,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140518",
									"weight": 2.3,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026141690",
									"weight": 2.45,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026142970",
									"weight": 0.7,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140044",
									"weight": 43,
									"quantity": 1,
									"signature_required": true
								},
								{
									"upc": "810026140051",
									"weight": 2,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140082",
									"weight": 22,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140181",
									"weight": 0.1875,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140198",
									"weight": 0.1875,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140204",
									"weight": 0.1875,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140211",
									"weight": 0.1875,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140396",
									"weight": 0.25,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026141980",
									"weight": 0.25,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026142017",
									"weight": 0.125,
									"quantity": 1,
									"signature_required": false
								}
							],
							"fulfillments": {
								"fulfillment_id": "740"
							},
							"shipping": {
								"exclusions": null
							}
						}
					]
				}
			}`)
		})

		pimclient = graphql.NewClient(
			"/query",
			&http.Client{Transport: localRoundTripper{handler: mux}},
		)

		o := model.Order{
			Items: []*model.Item{
				{
					ProductPid: 740,
				},
			},
		}

		err := getItemsPIMInfo(&o)
		assert.Nil(t, err)

		assert.Equal(t, 740, o.Items[0].ProductPid)
		assert.Equal(t, 740, o.Items[0].OriginalPid)
		assert.Equal(t, float64(74.675), o.Items[0].Weight)
	})

	t.Run("Not found", func(t *testing.T) {
		mux := http.NewServeMux()
		mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			mustWrite(w, `{
				"data": {
					"products": []
				}
			}`)
		})

		pimclient = graphql.NewClient(
			"/query",
			&http.Client{Transport: localRoundTripper{handler: mux}},
		)

		o := model.Order{
			Items: []*model.Item{
				{
					ProductPid: 1,
				},
			},
		}

		err := getItemsPIMInfo(&o)
		assert.Nil(t, err)

		assert.Equal(t, 1, o.Items[0].ProductPid)
		assert.Equal(t, 0, o.Items[0].OriginalPid)
	})
}

// localRoundTripper is an http.RoundTripper that executes HTTP transactions
// by using handler directly, instead of going over an HTTP connection.
type localRoundTripper struct {
	handler http.Handler
}

func (l localRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	w := httptest.NewRecorder()
	l.handler.ServeHTTP(w, req)
	return w.Result(), nil
}

func mustWrite(w io.Writer, s string) {
	_, err := io.WriteString(w, s)
	if err != nil {
		panic(err)
	}
}

func TestGetWeight(t *testing.T) {
	t.Run("Product Codes Weight", func(t *testing.T) {
		mux := http.NewServeMux()
		mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			mustWrite(w, `{
				"data": {
					"products": [
						{
							"product_id": "740",
							"brand": "pat",
							"product_codes": [
								{
									"upc": "810026140174",
									"weight": 0.8,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140488",
									"weight": 0.05,
									"quantity": 1,
									"signature_required": false
								},
								{
									"upc": "810026140518",
									"weight": 2.3,
									"quantity": 1,
									"signature_required": false
								}
							],
							"fulfillments": {
								"fulfillment_id": "740"
							},
							"shipping": {
								"exclusions": null
							}
						}
					]
				}
			}`)
		})

		pimclient = graphql.NewClient(
			"/query",
			&http.Client{Transport: localRoundTripper{handler: mux}},
		)

		o := model.Order{
			Items: []*model.Item{
				{
					ProductPid: 740,
				},
			},
		}

		err := getItemsPIMInfo(&o)
		assert.Nil(t, err)

		assert.Equal(t, 740, o.Items[0].ProductPid)
		assert.Equal(t, 740, o.Items[0].OriginalPid)
		assert.Equal(t, float64(3.15), o.Items[0].Weight)
	})

}
