package main

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
	"github.com/shurcooL/graphql"
)

type Product struct {
	ProductID    string          `graphql:"product_id" json:"product_id"`
	Brand        string          `json:"brand"`
	Fulfillments *Fulfillments   `json:"fulfillments"`
	ProductCodes []*ProductCodes `graphql:"product_codes" json:"product_codes"`
	Shipping     *Shipping       `graphql:"shipping" json:"shipping"`
}

type Fulfillments struct {
	FulfillmentID string `graphql:"fulfillment_id" json:"fulfillment_id"`
}

type ProductCodes struct {
	UPC               *string  `json:"upc"`
	Weight            *float64 `json:"weight"`
	Quantity          *int     `json:"quantity"`
	SignatureRequired *bool    `graphql:"signature_required" json:"signature_required"`
}

type Shipping struct {
	Exclusions []*string `graphql:"exclusions" json:"exclusions"`
}

var log *logger.Log
var pimclient *graphql.Client
var fhclient graphclient.FHGraphql

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}

	fhclient = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)
	pimclient = graphql.NewClient(os.Getenv("PIM_SERVICE_URL"), httpClient)
}

func getItemPIMInfo(item *model.Item, wg *sync.WaitGroup) (err error) {
	defer func() {
		if r := recover(); r != nil {
			switch x := r.(type) {
			case string:
				err = errors.New(x)
			case error:
				err = x
			default:
				err = fmt.Errorf("Error got, %v", r)
			}
		}
	}()
	defer wg.Done()

	var query struct {
		Products []Product `graphql:"products(product_id: $product_id, brand: $brand)" json:"products"`
	}

	brand := strings.ToLower(item.ProductBrand)
	variables := map[string]interface{}{
		"product_id": graphql.String(strconv.Itoa(item.ProductPid)),
		"brand":      graphql.String(brand),
	}

	err = pimclient.Query(context.Background(), &query, variables)
	if err != nil {
		log.Logger.Printf("PIM GraphQL query error %s ", err)
		return
	}

	log.Logger.Printf("PIM Response: %+v", query.Products)

	if len(query.Products) == 0 {
		// err = fmt.Errorf(
		// 	"Failed to find fulfillment ID for %d-%s",
		// 	item.ProductPid,
		// 	brand,
		// )
		log.Logger.Printf("Failed to find fulfillment ID for %d-%s", item.ProductPid, brand)
		return
	}

	// FulfillmentID
	fulfillmentID, err := strconv.Atoi(query.Products[0].Fulfillments.FulfillmentID)
	if err != nil {
		return
	}
	item.OriginalPid = item.ProductPid
	item.ProductPid = fulfillmentID

	// Weight
	weight := 0.0

	var skus []*model.Skus
	signatureRequired := false
	for _, productCodes := range query.Products[0].ProductCodes {
		if productCodes.SignatureRequired != nil {
			if *productCodes.SignatureRequired {
				signatureRequired = true
			}
		}

		if productCodes.Quantity == nil {
			return fmt.Errorf("Missing quantity for product %v", query.Products[0])
		}
		skus = append(skus, &model.Skus{
			Codes:    []string{*productCodes.UPC},
			Quantity: *productCodes.Quantity,
		})

		if productCodes.Weight != nil && productCodes.Quantity != nil {
			weight += *productCodes.Weight * float64(*productCodes.Quantity)
		}

	}

	item.Weight = weight
	item.SignatureRequired = signatureRequired
	item.Skus = skus

	return
}

func getItemsPIMInfo(order *model.Order) (err error) {
	var errors []error

	wg := new(sync.WaitGroup)
	for idx := range order.Items {
		wg.Add(1)
		go func(i int) {
			errors = append(errors, getItemPIMInfo(order.Items[i], wg))
		}(idx)
	}
	wg.Wait()

	for _, err := range errors {
		if err != nil {
			return err
		}
	}

	return
}

func handler(ctx context.Context, order model.Order) (model.Order, error) {
	var e error
	salesChannelOrderID := int64(order.SalesChannelOrderID)
	log, e = logger.NewOrderLog(&order.ID, &salesChannelOrderID, (*string)(&order.SalesChannel), (*string)(&order.Brand))
	if e != nil {
		return model.Order{}, e
	}
	log.Logger.Info("05-get-pim-data - start")

	if err := updateChildOrdersPIMInfo(&order); err != nil {
		errstr := err.Error()
		order.Error = &errstr
		order.Status = model.OrderStatusError
		return order, err
	}

	err := getItemsPIMInfo(&order)
	if err != nil {
		errstr := err.Error()
		order.Error = &errstr
		order.Status = model.OrderStatusError
	}

	return order, err
}

func updateChildOrdersPIMInfo(order *model.Order) error {
	log.Logger.Printf("Checking children orders for OrderID : %d", order.ID)

	var query = model.OrderFilterInput{
		ParentID: &order.ID,
	}

	o, err := fhclient.GetOrders(query)
	if err != nil {
		return err
	}
	log.Logger.Printf("Found %d children orders", len(o))

	var wg sync.WaitGroup

	var errors []error

	for _, child := range o {
		wg.Add(1)
		go func(order *model.Order) {
			log.Logger.Printf("Loading PIM info for child order %d", order.ID)
			defer wg.Done()
			err = getItemsPIMInfo(order)
			if err != nil {
				errors = append(errors, err)
			}
			fhclient.UpdateOrder(order)
		}(child)
	}

	wg.Wait()

	if len(errors) > 0 {
		log.Logger.Printf("Errors found : %v", errors)
		return errors[0]
	}

	return nil
}

func main() {
	lambda.Start(handler)
}
