package main

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

func init() {
	godotenv.Load("./../../.env")
}

/* func TestHandlerResendingAnOrder(t *testing.T) {
	orderJSON := `{
		"id": 353,
		"parentId": null,
		"brand": "PHA",
		"items": [
		  {
			"id": 747,
			"orderId": 353,
			"packageId": 204,
			"productPid": 401,
			"productBrand": "PHA",
			"originalPid": 401,
			"signatureRequired": false,
			"quantity": 1,
			"weight": 0.5,
			"skus": [
			  {
				"codes": [
				  "810026150739"
				],
				"quantity": 1
			  }
			],
			"trackingNumber": null,
			"createdAt": "2021-02-16T19:41:34Z",
			"updatedAt": "2021-02-16T19:43:27Z"
		  },
		  {
			"id": 748,
			"orderId": 353,
			"packageId": 204,
			"productPid": 648,
			"productBrand": "PHA",
			"originalPid": 0,
			"signatureRequired": false,
			"quantity": 1,
			"weight": 0,
			"skus": [
			  {
				"codes": [
				  "810026150999"
				],
				"quantity": 1
			  }
			],
			"trackingNumber": null,
			"createdAt": "2021-02-16T19:41:53Z",
			"updatedAt": "2021-02-16T19:43:27Z"
		  },
		  {
			"id": 749,
			"orderId": 353,
			"packageId": 204,
			"productPid": 646,
			"productBrand": "PHA",
			"originalPid": 0,
			"signatureRequired": false,
			"quantity": 1,
			"weight": 0,
			"skus": [
			  {
				"codes": [
				  "1810102"
				],
				"quantity": 1
			  }
			],
			"trackingNumber": null,
			"createdAt": "2021-02-16T19:41:53Z",
			"updatedAt": "2021-02-16T19:43:27Z"
		  }
		],
		"packages": [
		  {
			"id": 204,
			"orderId": 353,
			"vendorId": 1,
			"packageVendorId": "PHA2629016715333-0",
			"items": [
			  {
				"id": 747,
				"orderId": 353,
				"packageId": 204,
				"productPid": 401,
				"productBrand": "PHA",
				"originalPid": 401,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0.5,
				"skus": [
				  {
					"codes": [
					  "810026150739"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-02-16T19:41:34Z",
				"updatedAt": "2021-02-16T19:43:27Z"
			  },
			  {
				"id": 748,
				"orderId": 353,
				"packageId": 204,
				"productPid": 648,
				"productBrand": "PHA",
				"originalPid": 0,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0,
				"skus": [
				  {
					"codes": [
					  "810026150999"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-02-16T19:41:53Z",
				"updatedAt": "2021-02-16T19:43:27Z"
			  },
			  {
				"id": 749,
				"orderId": 353,
				"packageId": 204,
				"productPid": 646,
				"productBrand": "PHA",
				"originalPid": 0,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0,
				"skus": [
				  {
					"codes": [
					  "1810102"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-02-16T19:41:53Z",
				"updatedAt": "2021-02-16T19:43:27Z"
			  }
			],
			"weight": 0.5,
			"carrier": "",
			"status": "ERROR",
			"statusDescription": null,
			"estimatedDeliveryDate": null,
			"paused": null,
			"error": null,
			"shippingCarrier": "USPS",
			"shippingMethod": "First Class",
			"shippingCode": "USFCSP",
			"createdAt": "2021-02-16T19:41:53Z",
			"updatedAt": "2021-02-16T19:43:27Z",
			"is65prod": false,
			"information65propId": null,
			"signatureRequired": false,
			"vendorRequestPayload": "{\"shipaddress\":\"\",\"shipaddressee\":\"Marcio Oliveira\",\"shipaddress1\":\"noship\",\"shipaddress2\":\"\",\"shipcity\":\"San Francisco\",\"shipstate\":\"CA\",\"shipzip\":\"53333\",\"shipcountrycode\":\"US\",\"shipemail\":\"marcio.oliveira@client.com\",\"entity\":146470,\"otherrefnum\":\"PHA2629016715333-0\",\"memo\":\"\",\"shipmethod\":\"USFCSP\",\"products\":[{\"upc\":\"810026150739\",\"quantity\":1},{\"upc\":\"810026150999\",\"quantity\":1},{\"upc\":\"1810102\",\"quantity\":1}],\"custbody_class_code\":\"OTEST\"}",
			"vendorResponsePayload": "{\"message\":\"Resource could not be saved.\",\"errors\":{\"INVLD_SKU_UPC\":[\"The following UPCs are not found. Please submit valid UPCs. Not Found: 810026150739, 810026150999 and 1810102\"]},\"status_code\":422}"
		  }
		],
		"children": null,
		"firstname": "Marcio",
		"lastname": "Oliveira",
		"email": "marcio.oliveira@client.com",
		"phone": "",
		"street": "noship",
		"city": "San Francisco",
		"zip": "53333",
		"state": "CA",
		"country": "US",
		"salesChannel": "SHOPIFY",
		"salesChannelOrderId": 2629016715333,
		"status": "BUNDLED",
		"upgradedById": null,
		"upgradedAt": null,
		"paused": false,
		"pausedAt": null,
		"pauseReason": null,
		"resumedAt": null,
		"taskToken": null,
		"error": null,
		"fieldsUpdated": null,
		"createdAt": "2021-02-16T19:41:34Z",
		"updatedAt": "2021-02-16T19:43:27Z"
	  }`

	order := &model.Order{}
	json.Unmarshal([]byte(orderJSON), &order)

	o, err := handler(nil, *order)
	assert.Nil(t, err)
	assert.NotNil(t, o)
	assert.NotNil(t, o.Packages)
	assert.Equal(t, 1, len(o.Packages))
} */

func TestApplyRoutingRulesWithItemWithoutUPC(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, `{
			"data": {
				"orders": null
			}
		}`)
	})

	client = graphclient.NewFHGraphqlClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}},
	)

	orderJSON := `{
		"id": 644,
		"parentId": null,
		"brand": "PAT",
		"items": [
		  {
			"id": 1316,
			"orderId": 644,
			"packageId": null,
			"productPid": 927,
			"productBrand": "PAT",
			"originalPid": 927,
			"signatureRequired": false,
			"quantity": 1,
			"weight": 0,
			"skus": null,
			"trackingNumber": null,
			"createdAt": "2021-03-04T18:09:35Z",
			"updatedAt": "2021-03-04T18:11:37Z"
		  }
		],
		"packages": null,
		"children": null,
		"firstname": "steph3",
		"lastname": "test3",
		"email": "stephtest030400@test.com",
		"phone": "1111111111",
		"street": "noship3",
		"city": "Murfreesboro",
		"zip": "37128",
		"state": "TN",
		"country": "US",
		"salesChannel": "PORTAL",
		"salesChannelOrderId": 5558298,
		"status": "BUNDLED",
		"upgradedById": null,
		"upgradedAt": null,
		"paused": false,
		"pausedAt": null,
		"pauseReason": null,
		"resumedAt": null,
		"taskToken": null,
		"error": "",
		"fieldsUpdated": null,
		"createdAt": "2021-03-04T18:09:35Z",
		"updatedAt": "2021-03-04T18:11:37Z"
	  }`

	order := &model.Order{}
	json.Unmarshal([]byte(orderJSON), &order)

	err := applyRoutingRules(order)
	assert.Nil(t, err)
	assert.NotNil(t, order)
	assert.Nil(t, order.Packages)
}

func TestProportionRule(t *testing.T) {
	rMap := map[int]int{
		1: 30,
		3: 5,
		2: 50,
		4: 15,
	}
	r := createTestRule(rMap)

	todayMap := map[int]int{
		3: 6,
		1: 10,
		2: 2,
	}
	todayDistribution := createTestVendorDistribution(todayMap)

	vendorID, err := SelectVendorByProportion(r.RuleConditions, todayDistribution, 1)
	assert.Nil(t, err)
	assert.Equal(t, 2, vendorID)

}

func TestProportionRuleEmptyDistributions(t *testing.T) {
	rMap := map[int]int{
		1: 30,
		3: 5,
		2: 50,
		4: 15,
	}

	r := createTestRule(rMap)

	todayMap := map[int]int{}
	todayDistribution := createTestVendorDistribution(todayMap)

	vendorID, err := SelectVendorByProportion(r.RuleConditions, todayDistribution, 1)
	assert.Nil(t, err)
	assert.Equal(t, 2, vendorID)

}

func TestMaxCapRule(t *testing.T) {
	o := createTestOrder()

	rMap := map[int]maxCapRule{
		1: {VendorID: 4, MaxCap: 3},
		2: {VendorID: 3, MaxCap: 12},
		3: {VendorID: 2, MaxCap: 50},
		4: {VendorID: 1, MaxCap: -1},
	}

	r := createTestRuleWithPriority(rMap)

	todayMap := map[int]int{
		3: 5,
		1: 6,
	}
	todayDistribution := createTestVendorDistribution(todayMap)
	vdJSON, _ := json.MarshalIndent(&todayDistribution, "", "  ")
	log.Logger.Printf("Daily vendor distribution prior to routing ProductPID %d VendorDistributions %s\n", o.Items[0].ProductPid, string(vdJSON))

	vendorID, err := SelectVendorByMaxCap(r.RuleConditions, todayDistribution, o.Items[0].Quantity)
	assert.Nil(t, err)
	assert.Equal(t, 3, vendorID)

	o.Items[0].Quantity = 55
	vendorID, err = SelectVendorByMaxCap(r.RuleConditions, todayDistribution, o.Items[0].Quantity)
	assert.Nil(t, err)
	assert.Equal(t, 1, vendorID)
}

func TestMaxCapRuleEmptyDistributions(t *testing.T) {
	o := createTestOrder()

	rMap := map[int]maxCapRule{
		1: {VendorID: 4, MaxCap: 3},
		2: {VendorID: 3, MaxCap: 12},
		3: {VendorID: 2, MaxCap: 50},
		4: {VendorID: 1, MaxCap: -1},
	}

	r := createTestRuleWithPriority(rMap)

	todayMap := map[int]int{}
	todayDistribution := createTestVendorDistribution(todayMap)
	vdJSON, _ := json.MarshalIndent(&todayDistribution, "", "  ")
	log.Logger.Printf("Daily vendor distribution prior to routing ProductPID %d VendorDistributions %s\n", o.Items[0].ProductPid, string(vdJSON))

	vendorID, err := SelectVendorByMaxCap(r.RuleConditions, todayDistribution, o.Items[0].Quantity)
	assert.Nil(t, err)

	o.Items[0].Quantity = 55
	vendorID, err = SelectVendorByMaxCap(r.RuleConditions, todayDistribution, o.Items[0].Quantity)
	assert.Nil(t, err)
	assert.Equal(t, 1, vendorID)

}

func TestMapOrdersItems(t *testing.T) {
	o := createTestOrder()
	o.Items[0].Skus = append(o.Items[0].Skus, &model.Skus{
		Codes:    []string{"12345", "123"},
		Quantity: 1,
	})
	singleOrderItem := mapOrdersItems([]*model.Order{o})
	assert.Equal(t, o.Items[0].Skus[0].Codes[0], singleOrderItem[0].Skus[0].Codes[0])

	c := createCanceledTestOrder()
	twoOrders := createChildOrders()
	treeOrders := append(twoOrders, o)
	fourOrders := append(treeOrders, c)

	items := mapOrdersItems(fourOrders)
	assert.Equal(t, 3, len(items))
}

func TestCompleteFlow(t *testing.T) {
	t.Run("get Product", func(t *testing.T) {

		orderJSON := `{
				"id": 2,
				"status": "BUNDLED",
				"salesChannel": "NSP",
				"salesChannelOrderId": 5090706,
				"city": "Laredo",
				"packages": [],
				"items": [
					{
						"id": 2,
						"productPid": 817,
						"productBrand": "PAT",
						"quantity": 12,
						"productBrand": "4P"
					},
					{
						"id": 3,
						"productPid": 817,
						"productBrand": "PAT",
						"quantity": 100,
						"productBrand": "4P"
					},
					{
						"id": 4,
						"productPid": 817,
						"productBrand": "PAT",
						"quantity": 30,
						"productBrand": "4P"
					},
					{
						"id": 5,
						"productPid": 817,
						"productBrand": "PAT",
						"quantity": 1,
						"productBrand": "4P"
					},
					{
						"id": 6,
						"productPid": 817,
						"productBrand": "PAT",
						"quantity": 1,
						"productBrand": "4P"
					}
				],
				"parentId": 0,
				"email": "test@client.com",
				"phone": "(789) 123-4560",
				"firstname": "John",
				"lastname": "test"
			}`
		order := &model.Order{}
		json.Unmarshal([]byte(orderJSON), &order)

		var vendorPackage = make(map[int]model.Package)

		todayMap := map[int]int{
			3: 6,
			1: 10,
			2: 2,
		}

		for _, item := range order.Items {
			rMap := map[int]int{
				1: 30,
				3: 5,
				2: 50,
				4: 15,
			}

			rule := createTestRule(rMap)
			rule.RuleKind = model.RuleKindProp
			todayDistribution := createTestVendorDistribution(todayMap)

			var vendorID int
			var err error

			if rule.RuleKind == model.RuleKindProp {
				vendorID, err = SelectVendorByProportion(rule.RuleConditions, todayDistribution, item.Quantity)
				if err != nil {
					ruleConditionsJSON, _ := json.Marshal(rule.RuleConditions)
					log.Logger.Printf("Error getting vendor by Proportion - RuleConditions: %s Daily Distribution: %+v Item.Qty: %d Error: %+v", ruleConditionsJSON, todayDistribution, item.Quantity, err.Error())
				}
			} else {
				vendorID, err = SelectVendorByMaxCap(rule.RuleConditions, todayDistribution, item.Quantity)
				if err != nil {
					ruleConditionsJSON, _ := json.Marshal(rule.RuleConditions)
					log.Logger.Printf("Error getting vendor by MaxCap - RuleConditions: %s Daily Distribution: %+v Item.Qty: %d Error: %+v", ruleConditionsJSON, todayDistribution, item.Quantity, err.Error())
				}
			}

			// Create or Add item to vendor package
			if v, found := vendorPackage[vendorID]; found {
				addItem(&v, item)
				vendorPackage[vendorID] = v
			} else {
				vendorPackage[vendorID] = initializePackage(order, item, vendorID)
			}
			todayMap[vendorID] = (todayMap[vendorID] + item.Quantity)

			ID, qty, err := GetVendorDistributionIDAndQuantity(todayDistribution, vendorID, item.ProductPid, item.ProductBrand)
			if err == nil {
				log.Logger.Printf("Today´s vendor: %d distribution ID: %d for PID: %d Brand: %s updated from qty %d to qty: %d\n", vendorID, ID, item.ProductPid, item.ProductBrand, qty, qty+item.Quantity)
			}

		}

		// Add  packages to order
		for _, p := range vendorPackage {
			newP := p
			order.Packages = append(order.Packages, &newP)
		}
		packagesJSON, _ := json.Marshal(order.Packages)
		log.Logger.Printf("Order packages:\n%s\n", packagesJSON)
		assert.Equal(t, 3, len(order.Packages))
		assert.True(t, len(order.Packages[0].Items) > 0)
		assert.True(t, len(order.Packages[1].Items) > 0)
		assert.True(t, len(order.Packages[2].Items) > 0)
	})
}

func createTestOrder() (order *model.Order) {
	order = &model.Order{}
	item := &model.Item{
		ProductPid:   817,
		ProductBrand: "PAT",
		OriginalPid:  123,
		Quantity:     7,
	}

	order.Items = append(order.Items, item)
	return
}

func createCanceledTestOrder() (order *model.Order) {
	order = &model.Order{}
	order.Status = model.OrderStatusCanceled
	item := &model.Item{
		ProductPid:   666,
		ProductBrand: "pha",
		OriginalPid:  333,
		Quantity:     3,
	}

	order.Items = append(order.Items, item)
	return
}

func createChildOrders() (orders []*model.Order) {
	item1 := &model.Item{
		OrderID:      10,
		ProductPid:   17,
		ProductBrand: "PAT",
		OriginalPid:  123,
		Quantity:     2,
	}
	child1 := createTestOrder()
	child1.ID = 10
	child1.Items[0] = item1

	item2 := &model.Item{
		OrderID:      11,
		ProductPid:   333,
		ProductBrand: "PAT",
		OriginalPid:  456,
		Quantity:     5,
	}
	child2 := createTestOrder()
	child2.ID = 11
	child2.Items[0] = item2

	orders = append(orders, child1, child2)
	return
}

func createTestRule(i map[int]int) (r model.Rule) {
	r.RuleConditions = []*model.RuleCondition{}
	for k, v := range i {
		nv := v
		rc := &model.RuleCondition{
			Amount:   nv,
			VendorID: k,
		}
		r.RuleConditions = append(r.RuleConditions, rc)
	}
	return
}

func createTestVendorDistribution(i map[int]int) (vd []model.VendorDistribution) {
	for k, v := range i {
		tmp := model.VendorDistribution{
			ID:           k,
			Quantity:     v,
			VendorID:     k,
			ProductPid:   817,
			ProductBrand: "PAT",
		}
		vd = append(vd, tmp)
	}
	return
}

type roundTrip struct {
	h http.Handler
}

func (transport roundTrip) RoundTrip(req *http.Request) (*http.Response, error) {
	w := httptest.NewRecorder()
	transport.h.ServeHTTP(w, req)
	return w.Result(), nil
}

func mockGraphqlClient(response string) (c graphclient.FHGraphql) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_, err := w.Write([]byte(response))
		if err != nil {
			panic(err)
		}
	})

	c = graphclient.NewFHGraphqlClient(
		"/query",
		&http.Client{Transport: roundTrip{h: mux}},
	)
	return
}

func createTestRuleWithPriority(i map[int]maxCapRule) (r model.Rule) {
	r.RuleConditions = []*model.RuleCondition{}
	for k, v := range i {
		p := k
		rc := &model.RuleCondition{
			Amount:         v.MaxCap,
			VendorID:       v.VendorID,
			MaxCapPriority: &p,
		}
		r.RuleConditions = append(r.RuleConditions, rc)
	}
	return
}

func TestShouldNotify(t *testing.T) {

	orderNotified := map[int][]int{}

	okVendorID := 1
	okOrderID := 1234

	assert.False(t, findInSliceOrderID(orderNotified[okOrderID], okVendorID))
	log.Logger.Printf("Empty map: %+v\n", orderNotified)
	orderNotified[okOrderID] = append(orderNotified[okOrderID], okVendorID)
	assert.True(t, findInSliceOrderID(orderNotified[okOrderID], okVendorID))
	log.Logger.Printf("Populated map: %+v\n", orderNotified)

	absentVendorID := 2
	absentOrderID := 9999
	assert.False(t, findInSliceOrderID(orderNotified[okOrderID], absentVendorID))
	assert.False(t, findInSliceOrderID(orderNotified[absentOrderID], okVendorID))

	orderNotified[okOrderID] = append(orderNotified[okOrderID], absentVendorID)
	log.Logger.Printf("Updated map with Vendor ID: %d Populated map: %+v\n", absentVendorID, orderNotified)
	assert.True(t, findInSliceOrderID(orderNotified[okOrderID], absentVendorID))

}

// localRoundTripper is an http.RoundTripper that executes HTTP transactions
// by using handler directly, instead of going over an HTTP connection.
type localRoundTripper struct {
	handler http.Handler
}

func (l localRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	w := httptest.NewRecorder()
	l.handler.ServeHTTP(w, req)
	return w.Result(), nil
}

func mustWrite(w io.Writer, s string) {
	_, err := io.WriteString(w, s)
	if err != nil {
		panic(err)
	}
}
