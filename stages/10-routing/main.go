package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sort"
	"time"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
	snssender "github.com/client/fh-sns"
)

var log *logger.Log
var client graphclient.FHGraphql

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}
	client = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)
}

func handler(ctx context.Context, input model.Order) (order model.Order, err error) {
	salesChannelOrderID := int64(order.SalesChannelOrderID)
	log, err = logger.NewOrderLog(&input.ID, &salesChannelOrderID, (*string)(&input.SalesChannel), (*string)(&input.Brand))
	if err != nil {
		return model.Order{}, err
	}
	log.Logger.Info("10-routing - start")

	err = applyRoutingRules(&input)
	if err != nil {
		snssender.NotifyRoutingErrorEvent(&input, err)
	}
	order = input
	if order.Status != model.OrderStatusCanceled {
		order.Status = model.OrderStatusBundled
	}
	order.Error = nil

	for _, pkg := range order.Packages {
		if pkg.Status != model.PackageStatusCreating {
			continue
		}

		if pkg.Items == nil || len(pkg.Items) == 0 {
			errorMsg := fmt.Sprintf("Order %d, pkg %d has no items", order.SalesChannelOrderID, pkg.ID)
			log.Logger.Printf(errorMsg)
			log.Logger.Printf("Package %+v", pkg)
			order.Status = model.OrderStatusError
			order.Error = &errorMsg
			return order, errors.New(errorMsg)
		}

		for _, item := range pkg.Items {
			if item.Skus == nil || len(item.Skus) == 0 {
				errorMsg := fmt.Sprintf("Order %d, pid %d has empty UPC list", order.SalesChannelOrderID, item.ProductPid)
				log.Logger.Printf(errorMsg)
				log.Logger.Printf("Package %+v", pkg)
				order.Status = model.OrderStatusError
				order.Error = &errorMsg
				return order, errors.New(errorMsg)
			}
		}

		pkg.Status = model.PackageStatusCreated
		err := client.CreatePackage(pkg)
		if err != nil {
			return order, err
		}
	}

	return
}

func main() {
	lambda.Start(handler)
}

func applyRoutingRules(input *model.Order) (err error) {
	var vendorPackage = make(map[int]model.Package)
	var items []*model.Item
	var orders []*model.Order

	// get all child orders
	filter := model.OrderFilterInput{
		ParentID: &input.ID,
	}
	orders, err = client.GetOrders(filter)

	if err != nil {
		log.Logger.Printf("%+v", err.Error())
		return err
	}

	//Append current order
	orders = append(orders, input)

	// map all orders items Items
	items = mapOrdersItems(orders)
	orderNotified := map[int][]int{}
	for _, item := range items {
		if item.Skus == nil {
			continue
		}

		// Load distribution productVendorDistribution
		rule, err := client.GetRuleByProduct(*item)
		if err != nil {
			if err.Error() == "record not found" {
				err = fmt.Errorf("Routing rule not found for PID: %d (%s)", item.ProductPid, item.ProductBrand)
			}
			return err
		}

		// Load today´s distribution count for product vs all vendors
		todayDistribution, err := client.GetVendorDistribution(item.ProductPid, item.ProductBrand)
		if err != nil {
			err = fmt.Errorf("Error searching VendorDistribution ProductPid: %d Error: %+v", item.ProductPid, err.Error())
			return err
		}
		vdJSON, err := json.MarshalIndent(&todayDistribution, "", "  ")
		if err != nil {
			return err
		}
		log.Logger.Printf("Daily vendor distribution prior to routing ProductPID %d VendorDistributions %s\n", item.ProductPid, string(vdJSON))

		var vendorID int
		// Choose routing rule to apply based on Type
		if rule.RuleKind == model.RuleKindProp {
			vendorID, err = SelectVendorByProportion(rule.RuleConditions, todayDistribution, item.Quantity)
			if err != nil {
				ruleConditionsJSON, _ := json.Marshal(rule.RuleConditions)
				err = fmt.Errorf("Error getting vendor by Proportion - RuleConditions: %+v Daily Distribution: %+v Item.Qty: %d Error: %+v", ruleConditionsJSON, todayDistribution, item.Quantity, err.Error())
				return err
			}
		} else {
			vendorID, err = SelectVendorByMaxCap(rule.RuleConditions, todayDistribution, item.Quantity)
			if err != nil {
				ruleConditionsJSON, _ := json.Marshal(rule.RuleConditions)
				err = fmt.Errorf("Error getting vendor by MaxCap - RuleConditions: %+v Daily Distribution: %+v Item.Qty: %d Error: %+v", ruleConditionsJSON, todayDistribution, item.Quantity, err.Error())
				return err
			}
		}

		// Create or Add item to vendor package
		if v, found := vendorPackage[vendorID]; found {
			addItem(&v, item)
			vendorPackage[vendorID] = v
		} else {
			vendorPackage[vendorID] = initializePackage(input, item, vendorID)
		}

		// Update distribution count for vendor
		ID, _, err := GetVendorDistributionIDAndQuantity(todayDistribution, vendorID, item.ProductPid, item.ProductBrand)
		if err != nil {
			err = fmt.Errorf("Error: GetVendorDistributionID\n%+v", err.Error())
			return err
		}

		_, err = client.AddToVendorDistribution(ID, vendorID, item.ProductPid, item.ProductBrand, item.Quantity)
		if err != nil {
			err = fmt.Errorf("Error: AddToVendorDistribution\n%+v", err.Error())
			return err
		}

		// Notify SNS
		o := findOrderByID(orders, item.OrderID)
		vendor, err := client.GetVendor(vendorID)
		if err != nil {
			vendor.Name = fmt.Sprintf("VendorID: %d", vendorID)
		}

		// Check if the order ID is in the slice if not send notification
		if exist := findInSliceOrderID(orderNotified[item.OrderID], vendor.ID); exist == false {
			orderNotified[item.OrderID] = append(orderNotified[item.OrderID], vendor.ID)
			snssender.NotifyRoutingEvent(o, vendor.Name)
		}

	}

	// Add  packages to order
	for _, p := range vendorPackage {
		newP := p
		input.Packages = append(input.Packages, &newP)
	}

	return
}

// SelectVendorByProportion ...
func SelectVendorByProportion(conditions []*model.RuleCondition, todayDistribution []model.VendorDistribution, orderQty int) (vendorID int, err error) {
	// current stored proportion conditions
	desired := map[int]int{}
	// daily sales stats
	actual := map[int]int{}
	for _, v := range conditions {
		desired[v.VendorID] = v.Amount
		actual[v.VendorID] = 0
	}

	// collect daily stats + total
	total := 0
	for _, v := range todayDistribution {
		actual[v.VendorID] = v.Quantity
		total += v.Quantity
	}

	// get max prop vendor
	maxPropVendorID := -1
	p := -1
	for vendor, prop := range desired {
		if prop > p {
			p = prop
			maxPropVendorID = vendor
		}
	}

	// if daily total == 0 select > prop vendor
	if total == 0 {
		return maxPropVendorID, nil
	}

	// biggest difference should be selected
	// change here if qty item should affect current prop + dasily total
	vendorID = maxPropVendorID
	gap := -1
	for vendor, prop := range desired {
		cp := int((float64(actual[vendor]) / float64(total)) * 100)
		if (cp < prop) && ((prop - cp) > gap) {
			gap = prop - cp
			vendorID = vendor
		}
	}

	return
}

// SelectVendorByMaxCap ...
func SelectVendorByMaxCap(conditions []*model.RuleCondition, todayDistribution []model.VendorDistribution, orderQty int) (vendorID int, err error) {
	maxCapRules := map[int]maxCapRule{}
	skeys := []int{}
	for _, v := range conditions {
		maxCapRules[*v.MaxCapPriority] = maxCapRule{VendorID: v.VendorID, MaxCap: v.Amount}
		skeys = append(skeys, *v.MaxCapPriority)
	}
	sort.Ints(skeys)

	seek := []int{}
	for _, i := range skeys {
		seek = append(seek, i)
	}

	sorted := map[int]int{}
	skeys = []int{}
	for _, v := range todayDistribution {
		sorted[v.VendorID] = v.Quantity
		skeys = append(skeys, v.VendorID)
	}

	sort.Sort(sort.Reverse(sort.IntSlice(skeys)))

	current := []int{}
	for _, i := range skeys {
		current = append(current, i)

	}
	todayQty := sorted

	vendorID = -1
	for i := 1; i <= len(maxCapRules); i++ {
		v := maxCapRules[i]
		if todayQty[v.VendorID]+orderQty <= v.MaxCap {
			vendorID = v.VendorID
			break
		}
		if v.MaxCap <= 0 {
			vendorID = v.VendorID
			continue
		}
	}
	if vendorID == -1 {
		return vendorID, errors.New("no vendors were found to meet this quantity")
	}

	return
}

// GetVendorDistributionIDAndQuantity ...
func GetVendorDistributionIDAndQuantity(todayDistribution []model.VendorDistribution, vendorID int, ProductPid int, ProductBrand string) (ID int, qty int, err error) {
	for _, v := range todayDistribution {
		if (v.VendorID == vendorID) && (v.ProductPid == ProductPid) && (v.ProductBrand == ProductBrand) {
			ID = v.ID
			qty = v.Quantity
			break
		}
	}
	return
}

func initializePackage(o *model.Order, i *model.Item, vendorID int) (p model.Package) {
	log.Logger.Printf("Creating Package for VendorID %d\n", vendorID)
	location := time.FixedZone("America/Los_Angeles", -7*60*60)
	p.CreatedAt = time.Now().In(location).Format(time.RFC3339)

	p.OrderID = o.ID
	p.VendorID = vendorID
	p.Status = model.PackageStatusCreating

	addItem(&p, i)

	return
}

func addItem(p *model.Package, i *model.Item) {
	log.Logger.Printf("Adding Item PID %d OrderID: %d to Package: %d VendorID %d", i.ProductPid, i.OrderID, p.ID, p.VendorID)
	i.PackageID = &p.ID
	p.Items = append(p.Items, i)
	p.Weight = p.Weight + (i.Weight * float64(i.Quantity))

	log.Logger.Printf("OK\n")
}

func mapOrdersItems(orders []*model.Order) (items []*model.Item) {
	for _, order := range orders {

		if order.Status == model.OrderStatusCanceled {
			snssender.NotifyBundlingEvent(order)
			continue
		}

		if order.UpgradedByID != nil {
			continue
		}

		for _, item := range order.Items {
			if item.PackageID != nil {
				continue
			}
			OID := item.OrderID
			if OID == 0 {
				OID = order.ID
			}
			items = append(items, &model.Item{
				ID:                item.ID,
				ProductPid:        item.ProductPid,
				ProductBrand:      item.ProductBrand,
				OriginalPid:       item.OriginalPid,
				Quantity:          item.Quantity,
				Weight:            item.Weight,
				OrderID:           OID,
				PackageID:         item.PackageID,
				CreatedAt:         item.CreatedAt,
				UpdatedAt:         item.UpdatedAt,
				SignatureRequired: item.SignatureRequired,
				Skus:              item.Skus,
				TrackingNumber:    item.TrackingNumber,
			})
		}
	}
	JSON, _ := json.Marshal(items)
	log.Logger.Printf("Mapped Items: %s", JSON)
	return
}

func findOrderByID(orders []*model.Order, orderID int) (o *model.Order) {
	for _, order := range orders {
		if order.ID == orderID {
			o = order
			return
		}
	}
	return
}

// Check if the order ID is in the slice
func findInSliceOrderID(slice []int, id int) bool {
	for _, sliceID := range slice {
		if sliceID == id {
			return true
		}
	}
	return false
}

type maxCapRule struct {
	VendorID int
	MaxCap   int
}
