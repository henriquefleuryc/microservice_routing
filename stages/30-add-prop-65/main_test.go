package main

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

func init() {
	godotenv.Load("./../../.env")
}

func TestValidateIsProp65(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		body, _ := ioutil.ReadAll(req.Body)
		w.Header().Set("Content-Type", "application/json")
		castBody := string(body)
		if strings.Contains(castBody, "{\"query\":\"{order") {
			mustWrite(w, `{
				"data": {
					"Order": {
						"id": 65,
						"parentId": 0,
						"items": [
							{
								"id": 58,
								"productPid": 2,
								"productBrand": "PHA",
								"originalPid": 0,
								"quantity": 1,
								"orderId": 65,
								"packageId": null,
								"skus": [
									{
										"codes": [
											"810026140532",
											"810026150173"
										]
									}
								],
								"createdAt": "2020-11-30T19:19:25Z",
								"updatedAt": "2020-11-30T19:19:25Z"
							}
						],
						"packages": [
							{
								"id": 0,
								"orderId": 65,
								"vendorId": 2,
								"items": [
									{
										"id": 58,
										"productPid": 2,
										"productBrand": "PHA",
										"originalPid": 0,
										"quantity": 1,
										"orderId": 65,
										"packageId": 0,
										"skus": [
											{
												"codes": [
													"810026140532",
													"810026150173"
												],
												"quantity": 1
											}
										],
										"createdAt": "2020-11-30T12:19:27-07:00",
										"updatedAt": "2020-11-30T12:19:27-07:00"
									},
									{
										"id": 0,
										"productPid": 10001,
										"productBrand": "PHA",
										"originalPid": 0,
										"quantity": 1,
										"orderId": 0,
										"packageId": null,
										"createdAt": "",
										"updatedAt": ""
									},
									{
										"id": 0,
										"productPid": 10005,
										"productBrand": "PHA",
										"originalPid": 0,
										"quantity": 1,
										"orderId": 0,
										"packageId": null,
										"createdAt": "",
										"updatedAt": ""
									}
								],
								"weight": 0,
								"carrier": "",
								"status": "CREATED",
								"statusDescription": null,
								"estimatedDeliveryDate": null,
								"paused": null,
								"error": null,
								"createdAt": "2020-11-30T12:19:27-07:00",
								"updatedAt": "2020-11-30T12:19:27-07:00"
							}
						],
						"firstname": "Ricardo",
						"lastname": "Test",
						"email": "test7@client.com",
						"phone": "",
						"street": "noship",
						"city": "Los Angeles",
						"zip": "90037",
						"state": "CA",
						"country": "US",
						"salesChannel": "SHOPIFY",
						"salesChannelOrderId": 2907877933300,
						"status": "BUNDLED",
						"paused": null,
						"error": null,
						"createdAt": "0001-01-01T00:00:00Z",
						"updatedAt": "2020-11-30T19:19:25Z"
					}
				}
			}`)
		} else if strings.Contains(castBody, "{\"query\":\"{states65Prop{") {
			mustWrite(w, `{
			"data": {
				"states65Prop": [
					{
						"name": "CS",
						"textPHA": "Text for PHA product",
						"text4P": "Text for 4P product"
					},
					{
						"name": "CA",
						"textPHA": "Text for PHA product",
						"text4P": "Text for 4P product"
					}
				]
			}
		}`)
		} else if strings.Contains(castBody, "{\"query\":\"mutation($id:ID!$input:NewOrder!)") {
			mustWrite(w, `{
				"data": {
					"updateOrder": {
					  "id": 2,
					  "parentId": 0,
					  "is65Prod": true,
					  "information65PropId": 10
					}
				}
			}`)
		} else if strings.Contains(castBody, "{\"query\":\"mutation") {
			mustWrite(w, `{
				"data": {
					"information65Prop": {
						"id": 7,
						"skus": [
							{
								"id": 64,
								"productPid": 9876,
								"code": "Agora"
							},
							{
								"id": 65,
								"productPid": 12345,
								"code": "Mesmo"
							}
						],
						"text": "From Postman",
						"orderClassCode": "NOW"
					}
				}
			}`)
		} else if strings.Contains(castBody, "{\"query\":\"query($name:String!){state65Prop(") {
			mustWrite(w, `{
			"data": {
				"state65Prop": 
					{
						"name": "CA",
						"textPHA": "Text for PHA product",
						"text4P": "Text for 4P product"
					}
				}
			}`)
		} else {
			mustWrite(w, `{
				"data": {
					"skus65Prop": [
						{
							"id": 1,
							"productPid": 2,
							"code": "810026150173"
						},
						{
							"id": 2,
							"productPid": 3,
							"code": "810026150180"
						},
						{
							"id": 3,
							"productPid": 4,
							"code": "810026150739"
						},
						{
							"id": 4,
							"productPid": 5,
							"code": "810026150746"
						},
						{
							"id": 5,
							"productPid": 6,
							"code": "810026150753"
						},
						{
							"id": 6,
							"productPid": 7,
							"code": "810026150425"
						},
						{
							"id": 7,
							"productPid": 8,
							"code": "810026150944"
						},
						{
							"id": 8,
							"productPid": 9,
							"code": "810026150951"
						}
					]
				}
			}`)
		}
	})

	client = graphclient.NewFHGraphqlClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}})

	t.Run("default", func(t *testing.T) {
		packageID := 0

		packages := []*model.Package{
			{
				ID:       0,
				OrderID:  65,
				VendorID: 2,
				Weight:   0,
				Items: []*model.Item{
					{
						ID:           58,
						ProductPid:   2,
						ProductBrand: "PAT",
						Quantity:     1,
						OrderID:      65,
						PackageID:    &packageID,
						OriginalPid:  0,
						Skus: []*model.Skus{
							{
								Codes: []string{
									"810026140532",
									"810026150173",
								},
								Quantity: 1,
							},
						},
					},
					{
						ID:           2,
						ProductPid:   4,
						ProductBrand: "PHA",
						Quantity:     4,
						OrderID:      3,
						PackageID:    &packageID,
						Skus: []*model.Skus{
							{
								Codes: []string{
									"810026150739",
								},
								Quantity: 2,
							},
						},
					},
				},
			},
		}

		o := model.Order{
			ID:                  2,
			Firstname:           "FLEURY",
			Lastname:            "Cardoso",
			Email:               "henrique.fleury@client.com",
			Phone:               "12345678",
			Street:              "Test Street",
			City:                "Test",
			Zip:                 "098765443",
			State:               "CA",
			Country:             "US",
			SalesChannel:        "sales channel",
			SalesChannelOrderID: 1,
			Packages:            packages,
		}

		err := validateIsProp65(&o)

		assert.Nil(t, err)
		for _, p := range o.Packages {
			assert.True(t, *p.Is65prod)
		}
	})

}

func TestGetSkuProducts65Prop(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, `{
			"data": {
				"skus65Prop": [
					{
						"id": 1,
					"productPid": 2,
					"code": "810026150173"
					},
					{
						"id": 2,
					"productPid": 3,
					"code": "810026150180"
					},
					{
						"id": 3,
					"productPid": 4,
					"code": "810026150739"
					},
					{
						"id": 4,
					"productPid": 5,
					"code": "810026150746"
					},
					{
						"id": 5,
					"productPid": 6,
					"code": "810026150753"
					},
					{
						"id": 6,
					"productPid": 7,
					"code": "810026150425"
					},
					{
						"id": 7,
					"productPid": 8,
					"code": "810026150944"
					},
					{
						"id": 8,
					"productPid": 9,
					"code": "810026150951"
					}
				]
			}
		}`)

	})

	client = graphclient.NewFHGraphqlClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}})
	var productIDItems = []int{
		2, 7,
	}
	q, err := client.GetSkuProducts65Prop(productIDItems)

	assert.Nil(t, err)
	assert.Len(t, q, 2)
	value0 := *q[0]
	assert.Equal(t, "810026150173", value0.Code)
	value1 := *q[1]
	assert.Equal(t, "810026150425", value1.Code)
}

func TestGetStateInformation(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, `{
			"data": {
				"state65Prop": 
					{
						"name": "CA",
						"textPHA": "Text for PHA product",
						"text4P": "Text for 4P product"
					}
				
			}
		}`)
	})

	client = graphclient.NewFHGraphqlClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}})
	var state = "CA"
	q, err := client.GetState65PropInformation(state)

	assert.Nil(t, err)
	//statesText4p := *q.Text4p
	assert.Equal(t, "Text for 4P product", q.Text4p)
	//statesTextPha := *q.TextPha
	assert.Equal(t, "Text for PHA product", q.TextPha)
}

func TestGetListStates65Prop(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, `{
			"data": {
				"states65Prop": [
					{
						"name": "CS",
						"textPHA": "Text for PHA product",
						"text4P": "Text for 4P product"
					},
					{
						"name": "CA",
						"textPHA": "Text for PHA product",
						"text4P": "Text for 4P product"
					}
				]
			}
		}`)
	})

	client = graphclient.NewFHGraphqlClient("/query",
		&http.Client{Transport: localRoundTripper{handler: mux}})
	q, err := client.GetStates65Prop()

	assert.Nil(t, err)
	//statesCSText4p := *q[0].Text4p
	assert.Equal(t, "Text for 4P product", q[0].Text4p)
	//statesCATextPha := *q[1].TextPha
	assert.Equal(t, "Text for PHA product", q[1].TextPha)
}

func TestCreateInformation65Prop(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, `{
			"data": {
				"information65Prop": {
					"id": 7,
					"skus": [
						{
							"id": 64,
							"productPid": 9876,
							"code": "Agora"
						},
						{
							"id": 65,
							"productPid": 12345,
							"code": "Mesmo"
						}
					],
					"text": "From Postman",
					"orderClassCode": "NOW"
				}
			}
		}`)
	})

	client = graphclient.NewFHGraphqlClient("/query",
		&http.Client{Transport: localRoundTripper{handler: mux}})
	var skus []*model.Sku65Prop
	for i := 1; i < 3; i++ {
		skus = append(skus, &model.Sku65Prop{
			ProductPid: i,
			Code:       "Test Code",
		})
	}

	textInformation := "Test Text"
	orderClassInfor := "Order Class Test"
	informationProp := model.Information65Prop{
		OrderClassCode: &orderClassInfor,
		Text:           &textInformation,
		Skus:           skus,
	}

	information, err := client.CreateInformation65Prop(&informationProp)

	assert.Nil(t, err)
	informationID := *information
	assert.Equal(t, 7, informationID.ID)
}

// localRoundTripper is an http.RoundTripper that executes HTTP transactions
// by using handler directly, instead of going over an HTTP connection.
type localRoundTripper struct {
	handler http.Handler
}

func (l localRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	w := httptest.NewRecorder()
	l.handler.ServeHTTP(w, req)
	return w.Result(), nil
}

func mustWrite(w io.Writer, s string) {
	_, err := io.WriteString(w, s)
	if err != nil {
		panic(err)
	}
}
