package main

import (
	"context"
	"errors"
	"os"
	"strconv"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
)

var log *logger.Log
var client graphclient.FHGraphql

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}
	client = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)

}

func validateIsProp65(order *model.Order) error {
	listStates65Prop, err := client.GetStates65Prop()
	if err != nil {
		return err
	}

	isProp := false
	for _, state := range listStates65Prop {
		if order.State == state.Name {
			isProp = true
		}
	}

	if !isProp {
		return nil
	}

	for _, singlePackage := range order.Packages {
		if singlePackage.Is65prod != nil {
			continue
		}

		var productUpcItems []string
		for _, item := range singlePackage.Items {
			if len(item.Skus) == 0 {
				continue
			}

			for _, sku := range item.Skus {
				if sku.Quantity < 1 {
					continue
				}

				productUpcItems = append(productUpcItems, sku.Codes...)
			}
		}
		skus65Prod, err := client.GetSkuProducts65PropByUpc(productUpcItems)

		if err != nil {
			return err
		}

		if len(skus65Prod) == 0 {
			isProp = false
			singlePackage.Is65prod = &isProp
			return nil
		}
		isProp = true
		singlePackage.Is65prod = &isProp

		if err := addComplianceInformation(order, skus65Prod, singlePackage); err != nil {
			return errors.New("Error Adding Compliance Information")
		}
	}

	return nil
}

func addComplianceInformation(order *model.Order, skus65Prod []*model.Sku65Prop, p *model.Package) (err error) {
	stateInformation, err := client.GetState65PropInformation(order.State)
	if err != nil {
		return err
	}

	brand := getOrderBrand(order)

	var textInformation string
	if brand == "PHA" {
		textInformation = stateInformation.TextPha
	} else {
		textInformation = stateInformation.Text4p
	}

	orderClassInformation := strconv.Itoa(order.ID)

	information65Prop := &model.Information65Prop{
		Skus:           skus65Prod,
		Text:           &textInformation,
		OrderClassCode: &orderClassInformation,
	}

	informationProp, err := client.CreateInformation65Prop(information65Prop)
	if err != nil {
		return errors.New("Could not save the Infomation Prop 65")
	}

	p.Information65propID = &informationProp.ID

	return nil
}

func getOrderBrand(order *model.Order) (brand string) {

	for _, item := range order.Items {
		brand = string(item.ProductBrand)
	}

	return brand
}

func handler(ctx context.Context, order model.Order) (model.Order, error) {
	var err error
	salesChannelOrderID := int64(order.SalesChannelOrderID)
	log, err = logger.NewOrderLog(&order.ID, &salesChannelOrderID, (*string)(&order.SalesChannel), (*string)(&order.Brand))
	if err != nil {
		return model.Order{}, err
	}
	log.Logger.Info("30-add-prop-65 - start")

	err = validateIsProp65(&order)

	return order, err
}

func main() {
	lambda.Start(handler)
}
