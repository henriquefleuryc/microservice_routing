package main

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sort"
	"strings"
	"testing"

	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
	"github.com/joho/godotenv"
	"github.com/shurcooL/graphql"
	"github.com/stretchr/testify/assert"
)

func init() {
	godotenv.Load("./../../.env")
}

func TestGetUPCForPrintedCRM(t *testing.T) {
	assert.Equal(t, "18100488", getUPCForPrintedCRM(model.PrintedCrm{ProductPid: 938}))
	assert.Equal(t, "810026143465", getUPCForPrintedCRM(model.PrintedCrm{ProductPid: 939}))
	assert.Equal(t, "810026143472", getUPCForPrintedCRM(model.PrintedCrm{ProductPid: 940}))
	assert.Equal(t, "18101023", getUPCForPrintedCRM(model.PrintedCrm{ProductPid: 646}))
	assert.Equal(t, "810026150982", getUPCForPrintedCRM(model.PrintedCrm{ProductPid: 647}))
	assert.Equal(t, "810026150999", getUPCForPrintedCRM(model.PrintedCrm{ProductPid: 648}))
}

func TestFetchPrintedCRMs(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, `{
			"data": {
				"printedCRMs": [
					{
						"productPid": 9,
						"productBrand": "4P",
						"kind": "CATALOGUE",
						"states": [
							"CA"
						]
					},
					{
						"productPid": 648,
						"productBrand": "PHA",
						"kind": "CATALOGUE",
						"states": [
							"CA"
						]
					}
				]
			}
		}`)
	})

	client = graphclient.NewFHGraphqlClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}},
	)

	q, err := fetchPrintedCRMs()
	assert.Nil(t, err)
	assert.Len(t, q, 2)

	assert.Equal(t, 9, q[0].ProductPid)
	assert.Equal(t, "4P", q[0].ProductBrand)
	assert.Equal(t, model.PrintedCRMKind("CATALOGUE"), q[0].Kind)
	assert.Len(t, q[0].States, 1)
	assert.Equal(t, "CA", q[0].States[0])

	assert.Equal(t, 648, q[1].ProductPid)
	assert.Equal(t, "PHA", q[1].ProductBrand)
	assert.Equal(t, model.PrintedCRMKind("CATALOGUE"), q[1].Kind)
	assert.Len(t, q[1].States, 1)
	assert.Equal(t, "CA", q[1].States[0])
}

func TestContaisUPC(t *testing.T) {
	items := []*model.Item{
		{
			ProductPid:   999,
			ProductBrand: "PAT",
		},
	}

	assert.False(t, containsUPC(items, "810026143465"))

	items = append(items, &model.Item{
		ProductPid: 939,
		Skus:       []*model.Skus{{Codes: []string{"810026143465"}}},
	})

	assert.True(t, containsUPC(items, "810026143465"))
}

func TestAddPrintedCRMs(t *testing.T) {
	client = clientMock()

	t.Run("default", func(t *testing.T) {
		o := &model.Order{
			State: "TX",
			Brand: "PAT",
			Items: []*model.Item{
				{
					ProductPid:   999,
					ProductBrand: "PAT",
				},
			},
			Packages: []*model.Package{
				{
					Items: []*model.Item{
						{
							ProductPid:   999,
							ProductBrand: "PAT",
						},
					},
				},
			},
		}

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 3)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 938, o.Packages[0].Items[0].ProductPid)
		assert.Len(t, o.Packages[0].Items[0].Skus, 1)
		assert.Len(t, o.Packages[0].Items[0].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[0].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[0].Skus[0].Quantity)

		assert.Equal(t, 939, o.Packages[0].Items[1].ProductPid)
		assert.Len(t, o.Packages[0].Items[1].Skus, 1)
		assert.Len(t, o.Packages[0].Items[1].Skus[0].Codes, 1)
		assert.Equal(t, "810026143465", o.Packages[0].Items[1].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[1].Skus[0].Quantity)

		assert.Equal(t, 999, o.Packages[0].Items[2].ProductPid)
	})

	t.Run("specific state", func(t *testing.T) {
		o := &model.Order{
			State: "CA",
			Brand: "PAT",
			Items: []*model.Item{
				{
					ProductPid:   999,
					ProductBrand: "PAT",
				},
			},
			Packages: []*model.Package{
				{
					Items: []*model.Item{
						{
							ProductPid:   999,
							ProductBrand: "PAT",
						},
					},
				},
			},
		}

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 3)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 938, o.Packages[0].Items[0].ProductPid)
		assert.Len(t, o.Packages[0].Items[0].Skus, 1)
		assert.Len(t, o.Packages[0].Items[0].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[0].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[0].Skus[0].Quantity)

		assert.Equal(t, 940, o.Packages[0].Items[1].ProductPid)
		assert.Len(t, o.Packages[0].Items[1].Skus, 1)
		assert.Len(t, o.Packages[0].Items[1].Skus[0].Codes, 1)
		assert.Equal(t, "810026143472", o.Packages[0].Items[1].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[1].Skus[0].Quantity)

		assert.Equal(t, 999, o.Packages[0].Items[2].ProductPid)
	})

	t.Run("UPC 810026140044 should have PID 941 as Welcome Letter", func(t *testing.T) {
		o := &model.Order{
			State: "TX",
			Brand: "PAT",
			Items: []*model.Item{
				{
					ProductPid:   999,
					ProductBrand: "PAT",
					Skus:         []*model.Skus{{Codes: []string{"810026140044"}, Quantity: 1}},
				},
			},
			Packages: []*model.Package{
				{
					Items: []*model.Item{
						{
							ProductPid:   999,
							ProductBrand: "PAT",
							Skus:         []*model.Skus{{Codes: []string{"810026140044"}, Quantity: 1}},
						},
					},
				},
			},
		}

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 3)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 939, o.Packages[0].Items[0].ProductPid)
		assert.Len(t, o.Packages[0].Items[0].Skus, 1)
		assert.Len(t, o.Packages[0].Items[0].Skus[0].Codes, 1)
		assert.Equal(t, "810026143465", o.Packages[0].Items[0].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[0].Skus[0].Quantity)

		assert.Equal(t, 941, o.Packages[0].Items[1].ProductPid)
		assert.Equal(t, "PAT", o.Packages[0].Items[1].ProductBrand)
		assert.Len(t, o.Packages[0].Items[1].Skus, 1)
		assert.Len(t, o.Packages[0].Items[1].Skus[0].Codes, 1)
		assert.Equal(t, "18100884", o.Packages[0].Items[1].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[1].Skus[0].Quantity)

		assert.Equal(t, 999, o.Packages[0].Items[2].ProductPid)
	})

	t.Run("no catalog pids for PHA", func(t *testing.T) {
		o := &model.Order{
			State: "TX",
			Brand: "PHA",
			Items: []*model.Item{
				{
					ProductPid:   272,
					ProductBrand: "PHA",
				},
			},
			Packages: []*model.Package{
				{
					ID: 1,
					Items: []*model.Item{
						{
							ProductPid:   272,
							ProductBrand: "PHA",
						},
					},
				},
			},
		}

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 2)
	})

	t.Run("no catalog pids for PAT", func(t *testing.T) {
		o := &model.Order{
			State: "FL",
			Brand: "PAT",
			Items: []*model.Item{
				{
					ProductPid:   298,
					ProductBrand: "PAT",
				},
			},
			Packages: []*model.Package{
				{
					Items: []*model.Item{
						{
							ProductPid:   298,
							ProductBrand: "PAT",
						},
					},
				},
			},
		}

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 2) // only welcome letter
	})

	t.Run("no welcome letter or catalogs", func(t *testing.T) { //PIDs 146 and 234 in both noPrinted lists
		o := &model.Order{
			State: "TX",
			Items: []*model.Item{
				{
					ProductPid:   234,
					ProductBrand: "PAT",
				},
			},
			Packages: []*model.Package{
				{
					Items: []*model.Item{
						{
							ProductPid:   234,
							ProductBrand: "PAT",
						},
					},
				},
			},
		}

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 1)
	})

	t.Run("PID 18 bypassing welcome letter and catalog exclusions", func(t *testing.T) {
		o := &model.Order{
			State: "FL",
			Brand: "PAT",
			Items: []*model.Item{
				{
					ProductPid:   146,
					ProductBrand: "PAT",
				},
				{
					ProductPid:   18,
					ProductBrand: "PAT",
				},
			},
			Packages: []*model.Package{
				{
					Items: []*model.Item{
						{
							ProductPid:   18,
							ProductBrand: "PAT",
						},
						{
							ProductPid:   146,
							ProductBrand: "PAT",
						},
					},
				},
			},
		}

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 4)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 18, o.Packages[0].Items[0].ProductPid)

		assert.Equal(t, 146, o.Packages[0].Items[1].ProductPid)

		assert.Equal(t, 938, o.Packages[0].Items[2].ProductPid)
		assert.Len(t, o.Packages[0].Items[2].Skus, 1)
		assert.Len(t, o.Packages[0].Items[2].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[2].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[2].Skus[0].Quantity)

		assert.Equal(t, 939, o.Packages[0].Items[3].ProductPid)
		assert.Len(t, o.Packages[0].Items[3].Skus, 1)
		assert.Len(t, o.Packages[0].Items[3].Skus[0].Codes, 1)
		assert.Equal(t, "810026143465", o.Packages[0].Items[3].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[3].Skus[0].Quantity)
	})

	t.Run("PID 18 bypassing welcome letter and catalog exclusions JSON from routing step", func(t *testing.T) {
		oJSON := `{
			"id": 13306,
			"parentId": null,
			"brand": "PAT",
			"items": [
			  {
				"id": 45427,
				"orderId": 13306,
				"packageId": null,
				"productPid": 146,
				"productBrand": "PAT",
				"originalPid": 146,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0.1,
				"skus": [
				  {
					"codes": [
					  "810026140372"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-03-17T13:36:40Z",
				"updatedAt": "2021-03-17T13:36:40Z"
			  }
			],
			"packages": [
			  {
				"id": 12611,
				"orderId": 13306,
				"vendorId": 3,
				"packageVendorId": null,
				"items": [
				  {
					"id": 45428,
					"orderId": 13307,
					"packageId": 12611,
					"productPid": 18,
					"productBrand": "PAT",
					"originalPid": 18,
					"signatureRequired": true,
					"quantity": 1,
					"weight": 25,
					"skus": [
					  {
						"codes": [
						  "810026140532"
						],
						"quantity": 1
					  }
					],
					"trackingNumber": null,
					"createdAt": "2021-03-17T13:36:48Z",
					"updatedAt": "2021-03-17T13:36:48Z"
				  },
				  {
					"id": 45427,
					"orderId": 13306,
					"packageId": 12611,
					"productPid": 146,
					"productBrand": "PAT",
					"originalPid": 146,
					"signatureRequired": false,
					"quantity": 1,
					"weight": 0.1,
					"skus": [
					  {
						"codes": [
						  "810026140372"
						],
						"quantity": 1
					  }
					],
					"trackingNumber": null,
					"createdAt": "2021-03-17T13:36:48Z",
					"updatedAt": "2021-03-17T13:36:48Z"
				  }
				],
				"weight": 25.1,
				"carrier": "",
				"status": "CREATED",
				"statusDescription": null,
				"estimatedDeliveryDate": null,
				"paused": null,
				"error": null,
				"shippingCarrier": "",
				"shippingMethod": "",
				"shippingCode": "",
				"createdAt": "2021-03-17T13:36:48Z",
				"updatedAt": "2021-03-17T13:36:48Z",
				"is65prod": null,
				"information65propId": null,
				"signatureRequired": true,
				"vendorRequestPayload": null,
				"vendorResponsePayload": null
			  }
			],
			"children": [],
			"firstname": "ASDF",
			"lastname": "test",
			"email": "a@b.com",
			"phone": "(251) 675-4044",
			"street": "noship",
			"city": "Racine",
			"zip": "58876",
			"state": "FL",
			"country": "US",
			"salesChannel": "NSP",
			"salesChannelOrderId": 15608510,
			"status": "BUNDLED",
			"upgradedById": null,
			"upgradedAt": null,
			"paused": false,
			"pausedAt": null,
			"pauseReason": null,
			"resumedAt": null,
			"taskToken": null,
			"error": null,
			"fieldsUpdated": null,
			"createdAt": "0001-01-01T00:00:00Z",
			"updatedAt": "2021-03-17T13:36:40Z"
		  }`

		o := &model.Order{}
		_ = json.Unmarshal([]byte(oJSON), o)

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 4)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 18, o.Packages[0].Items[0].ProductPid)

		assert.Equal(t, 146, o.Packages[0].Items[1].ProductPid)

		assert.Equal(t, 938, o.Packages[0].Items[2].ProductPid)
		assert.Len(t, o.Packages[0].Items[2].Skus, 1)
		assert.Len(t, o.Packages[0].Items[2].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[2].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[2].Skus[0].Quantity)

		assert.Equal(t, 939, o.Packages[0].Items[3].ProductPid)
		assert.Len(t, o.Packages[0].Items[3].Skus, 1)
		assert.Len(t, o.Packages[0].Items[3].Skus[0].Codes, 1)
		assert.Equal(t, "810026143465", o.Packages[0].Items[3].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[3].Skus[0].Quantity)
	})

	t.Run("PID 18 (146 first) bypassing welcome letter and catalog exclusions JSON from routing step", func(t *testing.T) {
		oJSON := `{
			"id": 13306,
			"parentId": null,
			"brand": "PAT",
			"items": [
			  {
				"id": 45427,
				"orderId": 13306,
				"packageId": null,
				"productPid": 18,
				"productBrand": "PAT",
				"originalPid": 18,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0.1,
				"skus": [
				  {
					"codes": [
					  "810026140372"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-03-17T13:36:40Z",
				"updatedAt": "2021-03-17T13:36:40Z"
			  }
			],
			"packages": [
			  {
				"id": 12611,
				"orderId": 13306,
				"vendorId": 3,
				"packageVendorId": null,
				"items": [
				  {
					"id": 45427,
					"orderId": 13306,
					"packageId": null,
					"productPid": 146,
					"productBrand": "PAT",
					"originalPid": 146,
					"signatureRequired": false,
					"quantity": 1,
					"weight": 0.1,
					"skus": [
					{
						"codes": [
						"810026140372"
						],
						"quantity": 1
					}
					],
					"trackingNumber": null,
					"createdAt": "2021-03-17T13:36:40Z",
					"updatedAt": "2021-03-17T13:36:40Z"
				  },
				  {
					"id": 45428,
					"orderId": 13307,
					"packageId": 12611,
					"productPid": 18,
					"productBrand": "PAT",
					"originalPid": 18,
					"signatureRequired": true,
					"quantity": 1,
					"weight": 25,
					"skus": [
					  {
						"codes": [
						  "810026140532"
						],
						"quantity": 1
					  }
					],
					"trackingNumber": null,
					"createdAt": "2021-03-17T13:36:48Z",
					"updatedAt": "2021-03-17T13:36:48Z"					  
				  }
				],
				"weight": 25.1,
				"carrier": "",
				"status": "CREATED",
				"statusDescription": null,
				"estimatedDeliveryDate": null,
				"paused": null,
				"error": null,
				"shippingCarrier": "",
				"shippingMethod": "",
				"shippingCode": "",
				"createdAt": "2021-03-17T13:36:48Z",
				"updatedAt": "2021-03-17T13:36:48Z",
				"is65prod": null,
				"information65propId": null,
				"signatureRequired": true,
				"vendorRequestPayload": null,
				"vendorResponsePayload": null
			  }
			],
			"children": [],
			"firstname": "ASDF",
			"lastname": "test",
			"email": "a@b.com",
			"phone": "(251) 675-4044",
			"street": "noship",
			"city": "Racine",
			"zip": "58876",
			"state": "FL",
			"country": "US",
			"salesChannel": "NSP",
			"salesChannelOrderId": 15608510,
			"status": "BUNDLED",
			"upgradedById": null,
			"upgradedAt": null,
			"paused": false,
			"pausedAt": null,
			"pauseReason": null,
			"resumedAt": null,
			"taskToken": null,
			"error": null,
			"fieldsUpdated": null,
			"createdAt": "0001-01-01T00:00:00Z",
			"updatedAt": "2021-03-17T13:36:40Z"
		  }`

		o := &model.Order{}
		_ = json.Unmarshal([]byte(oJSON), o)

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 4)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 18, o.Packages[0].Items[0].ProductPid)

		assert.Equal(t, 146, o.Packages[0].Items[1].ProductPid)

		assert.Equal(t, 938, o.Packages[0].Items[2].ProductPid)
		assert.Len(t, o.Packages[0].Items[2].Skus, 1)
		assert.Len(t, o.Packages[0].Items[2].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[2].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[2].Skus[0].Quantity)

		assert.Equal(t, 939, o.Packages[0].Items[3].ProductPid)
		assert.Len(t, o.Packages[0].Items[3].Skus, 1)
		assert.Len(t, o.Packages[0].Items[3].Skus[0].Codes, 1)
		assert.Equal(t, "810026143465", o.Packages[0].Items[3].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[3].Skus[0].Quantity)
	})

	t.Run("PID 17 bypassing welcome letter exclusion JSON from routing step", func(t *testing.T) {
		oJSON := `{
			"id": 13306,
			"parentId": null,
			"brand": "PAT",
			"items": [
			  {
				"id": 45435,
				"orderId": 13311,
				"packageId": 12614,
				"productPid": 146,
				"productBrand": "PAT",
				"originalPid": 146,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0.1,
				"skus": [
				  {
					"codes": [
					  "810026140372"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-03-17T14:05:49Z",
				"updatedAt": "2021-03-17T14:11:05Z"
			  }
			],
			"packages": [
			  {
				"id": 12611,
				"orderId": 13306,
				"vendorId": 3,
				"packageVendorId": null,
				"items": [
					{
						"id": 45436,
						"orderId": 13311,
						"packageId": 12615,
						"productPid": 17,
						"productBrand": "PAT",
						"originalPid": 17,
						"signatureRequired": false,
						"quantity": 1,
						"weight": 2.3,
						"skus": [
						  {
							"codes": [
							  "810026140518"
							],
							"quantity": 1
						  }
						],
						"trackingNumber": null,
						"createdAt": "2021-03-17T14:05:49Z",
						"updatedAt": "2021-03-17T14:11:08Z"
					},
					{
						"id": 45435,
						"orderId": 13311,
						"packageId": 12614,
						"productPid": 146,
						"productBrand": "PAT",
						"originalPid": 146,
						"signatureRequired": false,
						"quantity": 1,
						"weight": 0.1,
						"skus": [
						  {
							"codes": [
							  "810026140372"
							],
							"quantity": 1
						  }
						],
						"trackingNumber": null,
						"createdAt": "2021-03-17T14:05:49Z",
						"updatedAt": "2021-03-17T14:11:05Z"
					}
				],
				"weight": 25.1,
				"carrier": "",
				"status": "CREATED",
				"statusDescription": null,
				"estimatedDeliveryDate": null,
				"paused": null,
				"error": null,
				"shippingCarrier": "",
				"shippingMethod": "",
				"shippingCode": "",
				"createdAt": "2021-03-17T13:36:48Z",
				"updatedAt": "2021-03-17T13:36:48Z",
				"is65prod": null,
				"information65propId": null,
				"signatureRequired": true,
				"vendorRequestPayload": null,
				"vendorResponsePayload": null
			  }
			],
			"children": [],
			"firstname": "ASDF",
			"lastname": "test",
			"email": "a@b.com",
			"phone": "(251) 675-4044",
			"street": "noship",
			"city": "Racine",
			"zip": "58876",
			"state": "FL",
			"country": "US",
			"salesChannel": "NSP",
			"salesChannelOrderId": 15608510,
			"status": "BUNDLED",
			"upgradedById": null,
			"upgradedAt": null,
			"paused": false,
			"pausedAt": null,
			"pauseReason": null,
			"resumedAt": null,
			"taskToken": null,
			"error": null,
			"fieldsUpdated": null,
			"createdAt": "0001-01-01T00:00:00Z",
			"updatedAt": "2021-03-17T13:36:40Z"
		  }`

		o := &model.Order{}
		_ = json.Unmarshal([]byte(oJSON), o)

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 3)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 17, o.Packages[0].Items[0].ProductPid)

		assert.Equal(t, 146, o.Packages[0].Items[1].ProductPid)

		assert.Equal(t, 938, o.Packages[0].Items[2].ProductPid)
		assert.Len(t, o.Packages[0].Items[2].Skus, 1)
		assert.Len(t, o.Packages[0].Items[2].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[2].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[2].Skus[0].Quantity)
	})

	t.Run("PID 17 (146 first) bypassing welcome letter exclusion JSON from routing step", func(t *testing.T) {
		oJSON := `{
			"id": 13306,
			"parentId": null,
			"brand": "PAT",
			"items": [
			  {
				"id": 45436,
				"orderId": 13311,
				"packageId": 12615,
				"productPid": 17,
				"productBrand": "PAT",
				"originalPid": 17,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 2.3,
				"skus": [
				  {
					"codes": [
					  "810026140518"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-03-17T14:05:49Z",
				"updatedAt": "2021-03-17T14:11:08Z"
			  }
			],
			"packages": [
			  {
				"id": 12611,
				"orderId": 13306,
				"vendorId": 3,
				"packageVendorId": null,
				"items": [
					{
						"id": 45435,
						"orderId": 13311,
						"packageId": 12614,
						"productPid": 146,
						"productBrand": "PAT",
						"originalPid": 146,
						"signatureRequired": false,
						"quantity": 1,
						"weight": 0.1,
						"skus": [
						  {
							"codes": [
							  "810026140372"
							],
							"quantity": 1
						  }
						],
						"trackingNumber": null,
						"createdAt": "2021-03-17T14:05:49Z",
						"updatedAt": "2021-03-17T14:11:05Z"						
					},
					{
						"id": 45436,
						"orderId": 13311,
						"packageId": 12615,
						"productPid": 17,
						"productBrand": "PAT",
						"originalPid": 17,
						"signatureRequired": false,
						"quantity": 1,
						"weight": 2.3,
						"skus": [
						  {
							"codes": [
							  "810026140518"
							],
							"quantity": 1
						  }
						],
						"trackingNumber": null,
						"createdAt": "2021-03-17T14:05:49Z",
						"updatedAt": "2021-03-17T14:11:08Z"
					}
				],
				"weight": 25.1,
				"carrier": "",
				"status": "CREATED",
				"statusDescription": null,
				"estimatedDeliveryDate": null,
				"paused": null,
				"error": null,
				"shippingCarrier": "",
				"shippingMethod": "",
				"shippingCode": "",
				"createdAt": "2021-03-17T13:36:48Z",
				"updatedAt": "2021-03-17T13:36:48Z",
				"is65prod": null,
				"information65propId": null,
				"signatureRequired": true,
				"vendorRequestPayload": null,
				"vendorResponsePayload": null
			  }
			],
			"children": [],
			"firstname": "ASDF",
			"lastname": "test",
			"email": "a@b.com",
			"phone": "(251) 675-4044",
			"street": "noship",
			"city": "Racine",
			"zip": "58876",
			"state": "FL",
			"country": "US",
			"salesChannel": "NSP",
			"salesChannelOrderId": 15608510,
			"status": "BUNDLED",
			"upgradedById": null,
			"upgradedAt": null,
			"paused": false,
			"pausedAt": null,
			"pauseReason": null,
			"resumedAt": null,
			"taskToken": null,
			"error": null,
			"fieldsUpdated": null,
			"createdAt": "0001-01-01T00:00:00Z",
			"updatedAt": "2021-03-17T13:36:40Z"
		  }`

		o := &model.Order{}
		_ = json.Unmarshal([]byte(oJSON), o)

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 3)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 17, o.Packages[0].Items[0].ProductPid)

		assert.Equal(t, 146, o.Packages[0].Items[1].ProductPid)

		assert.Equal(t, 938, o.Packages[0].Items[2].ProductPid)
		assert.Len(t, o.Packages[0].Items[2].Skus, 1)
		assert.Len(t, o.Packages[0].Items[2].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[2].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[2].Skus[0].Quantity)
	})

	t.Run("Package with PID 300 should only contain items 300 & 938 (WL) catalog exclusion JSON from routing step", func(t *testing.T) {
		oJSON := `{
			"id": 13331,
			"parentId": null,
			"brand": "PAT",
			"items": [
				{
				"id": 45474,
				"orderId": 13331,
				"packageId": null,
				"productPid": 610,
				"productBrand": "PAT",
				"originalPid": 610,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 15.9,
				"skus": [
					{
					"codes": [
					"810026141768"
					],
					"quantity": 1
					}
				],
				"trackingNumber": null,
				"createdAt": "2021-03-18T16:50:59Z",
				"updatedAt": "2021-03-18T16:50:59Z"
				}
			],
			"packages": [
				{
				"id": 12624,
				"orderId": 13331,
				"vendorId": 3,
				"packageVendorId": null,
				"items": [
					{
					"id": 45475,
					"orderId": 13332,
					"packageId": 12624,
					"productPid": 300,
					"productBrand": "PAT",
					"originalPid": 300,
					"signatureRequired": false,
					"quantity": 1,
					"weight": 1,
					"skus": [
						{
						"codes": [
						"810026140600"
						],
						"quantity": 1
						}
					],
					"trackingNumber": null,
					"createdAt": "2021-03-18T16:51:05Z",
					"updatedAt": "2021-03-18T16:51:05Z"
					}
				],
				"weight": 1,
				"carrier": "",
				"status": "CREATED",
				"statusDescription": null,
				"estimatedDeliveryDate": null,
				"paused": null,
				"error": null,
				"shippingCarrier": "",
				"shippingMethod": "",
				"shippingCode": "",
				"createdAt": "2021-03-18T16:51:05Z",
				"updatedAt": "2021-03-18T16:51:05Z",
				"is65prod": null,
				"information65propId": null,
				"signatureRequired": false,
				"vendorRequestPayload": null,
				"vendorResponsePayload": null
				},
				{
				"id": 12625,
				"orderId": 13331,
				"vendorId": 4,
				"packageVendorId": null,
				"items": [
					{
					"id": 45474,
					"orderId": 13331,
					"packageId": 12625,
					"productPid": 610,
					"productBrand": "PAT",
					"originalPid": 610,
					"signatureRequired": false,
					"quantity": 1,
					"weight": 15.9,
					"skus": [
						{
						"codes": [
						"810026141768"
						],
						"quantity": 1
						}
					],
					"trackingNumber": null,
					"createdAt": "2021-03-18T16:51:05Z",
					"updatedAt": "2021-03-18T16:51:05Z"
					}
				],
				"weight": 15.9,
				"carrier": "",
				"status": "CREATED",
				"statusDescription": null,
				"estimatedDeliveryDate": null,
				"paused": null,
				"error": null,
				"shippingCarrier": "",
				"shippingMethod": "",
				"shippingCode": "",
				"createdAt": "2021-03-18T16:51:05Z",
				"updatedAt": "2021-03-18T16:51:05Z",
				"is65prod": null,
				"information65propId": null,
				"signatureRequired": false,
				"vendorRequestPayload": null,
				"vendorResponsePayload": null
				}
			],
			"children": [],
			"firstname": "Ricardo",
			"lastname": "Test",
			"email": "RicardoTest1616082442@test.com",
			"phone": "7891234560",
			"street": "noship",
			"city": "Orlando",
			"zip": "12345",
			"state": "FL",
			"country": "US",
			"salesChannel": "PORTAL",
			"salesChannelOrderId": 5613002,
			"status": "BUNDLED",
			"upgradedById": null,
			"upgradedAt": null,
			"paused": false,
			"pausedAt": null,
			"pauseReason": null,
			"resumedAt": null,
			"taskToken": null,
			"error": null,
			"fieldsUpdated": null,
			"createdAt": "0001-01-01T00:00:00Z",
			"updatedAt": "2021-03-18T16:50:59Z"
		}`

		o := &model.Order{}
		_ = json.Unmarshal([]byte(oJSON), o)

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 2)
		assert.Len(t, o.Packages[1].Items, 3)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})
		sort.Slice(o.Packages[1].Items, func(i, j int) bool {
			return o.Packages[1].Items[i].ProductPid < o.Packages[1].Items[j].ProductPid
		})

		assert.Equal(t, 300, o.Packages[0].Items[0].ProductPid)

		assert.Equal(t, 938, o.Packages[0].Items[1].ProductPid)
		assert.Len(t, o.Packages[0].Items[1].Skus, 1)
		assert.Len(t, o.Packages[0].Items[1].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[1].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[1].Skus[0].Quantity)

		assert.Equal(t, 610, o.Packages[1].Items[0].ProductPid)

		assert.Equal(t, 938, o.Packages[1].Items[1].ProductPid)
		assert.Len(t, o.Packages[1].Items[1].Skus, 1)
		assert.Len(t, o.Packages[1].Items[1].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[1].Items[1].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[1].Items[1].Skus[0].Quantity)

		assert.Equal(t, 939, o.Packages[1].Items[2].ProductPid)
		assert.Len(t, o.Packages[1].Items[2].Skus, 1)
		assert.Len(t, o.Packages[1].Items[2].Skus[0].Codes, 1)
		assert.Equal(t, "810026143465", o.Packages[1].Items[2].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[1].Items[2].Skus[0].Quantity)
	})

	t.Run("PID 17 (146 first) bypassing welcome letter exclusion multiple packages JSON from routing step", func(t *testing.T) {
		oJSON := `{
			"id": 13334,
			"parentId": null,
			"brand": "PAT",
			"items": [
			  {
				"id": 45483,
				"orderId": 13334,
				"packageId": null,
				"productPid": 146,
				"productBrand": "PAT",
				"originalPid": 146,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0.1,
				"skus": [
				  {
					"codes": [
					  "810026140372"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-03-18T17:02:54Z",
				"updatedAt": "2021-03-18T17:02:54Z"
			  }
			],
			"packages": [
			  {
				"id": 12627,
				"orderId": 13334,
				"vendorId": 2,
				"packageVendorId": null,
				"items": [
				  {
					"id": 45484,
					"orderId": 13335,
					"packageId": 12627,
					"productPid": 17,
					"productBrand": "PAT",
					"originalPid": 17,
					"signatureRequired": false,
					"quantity": 1,
					"weight": 2.3,
					"skus": [
					  {
						"codes": [
						  "810026140518"
						],
						"quantity": 1
					  }
					],
					"trackingNumber": null,
					"createdAt": "2021-03-18T17:02:58Z",
					"updatedAt": "2021-03-18T17:02:58Z"
				  }
				],
				"weight": 2.3,
				"carrier": "",
				"status": "CREATED",
				"statusDescription": null,
				"estimatedDeliveryDate": null,
				"paused": null,
				"error": null,
				"shippingCarrier": "",
				"shippingMethod": "",
				"shippingCode": "",
				"createdAt": "2021-03-18T17:02:58Z",
				"updatedAt": "2021-03-18T17:02:58Z",
				"is65prod": null,
				"information65propId": null,
				"signatureRequired": false,
				"vendorRequestPayload": null,
				"vendorResponsePayload": null
			  },
			  {
				"id": 12628,
				"orderId": 13334,
				"vendorId": 3,
				"packageVendorId": null,
				"items": [
				  {
					"id": 45483,
					"orderId": 13334,
					"packageId": 12628,
					"productPid": 146,
					"productBrand": "PAT",
					"originalPid": 146,
					"signatureRequired": false,
					"quantity": 1,
					"weight": 0.1,
					"skus": [
					  {
						"codes": [
						  "810026140372"
						],
						"quantity": 1
					  }
					],
					"trackingNumber": null,
					"createdAt": "2021-03-18T17:02:58Z",
					"updatedAt": "2021-03-18T17:02:58Z"
				  }
				],
				"weight": 0.1,
				"carrier": "",
				"status": "CREATED",
				"statusDescription": null,
				"estimatedDeliveryDate": null,
				"paused": null,
				"error": null,
				"shippingCarrier": "",
				"shippingMethod": "",
				"shippingCode": "",
				"createdAt": "2021-03-18T17:02:58Z",
				"updatedAt": "2021-03-18T17:02:58Z",
				"is65prod": null,
				"information65propId": null,
				"signatureRequired": false,
				"vendorRequestPayload": null,
				"vendorResponsePayload": null
			  }
			],
			"children": [],
			"firstname": "ASDF",
			"lastname": "test",
			"email": "a@b.com",
			"phone": "(251) 675-4044",
			"street": "noship",
			"city": "Racine",
			"zip": "58876",
			"state": "WI",
			"country": "US",
			"salesChannel": "NSP",
			"salesChannelOrderId": 35608520,
			"status": "BUNDLED",
			"upgradedById": null,
			"upgradedAt": null,
			"paused": false,
			"pausedAt": null,
			"pauseReason": null,
			"resumedAt": null,
			"taskToken": null,
			"error": null,
			"fieldsUpdated": null,
			"createdAt": "0001-01-01T00:00:00Z",
			"updatedAt": "2021-03-18T17:02:54Z"
		  }`

		o := &model.Order{}
		_ = json.Unmarshal([]byte(oJSON), o)

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 2)
		assert.Len(t, o.Packages[1].Items, 1)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})
		sort.Slice(o.Packages[1].Items, func(i, j int) bool {
			return o.Packages[1].Items[i].ProductPid < o.Packages[1].Items[j].ProductPid
		})

		assert.Equal(t, 17, o.Packages[0].Items[0].ProductPid)

		assert.Equal(t, 938, o.Packages[0].Items[1].ProductPid)
		assert.Len(t, o.Packages[0].Items[1].Skus, 1)
		assert.Len(t, o.Packages[0].Items[1].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[1].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[1].Skus[0].Quantity)

		assert.Equal(t, 146, o.Packages[1].Items[0].ProductPid)
	})

	t.Run("only welcome letter, catalog exclusion", func(t *testing.T) {
		o := &model.Order{
			State: "TX",
			Brand: "PAT",
			Items: []*model.Item{
				{
					ProductPid:   234,
					ProductBrand: "PAT",
				},
				{
					ProductPid:   298,
					ProductBrand: "PAT",
				},
			},
			Packages: []*model.Package{
				{
					Items: []*model.Item{
						{
							ProductPid:   234,
							ProductBrand: "PAT",
						},
						{
							ProductPid:   298,
							ProductBrand: "PAT",
						},
					},
				},
			},
		}

		err := addPrintedCRMs(o)
		assert.Nil(t, err)
		assert.Len(t, o.Packages[0].Items, 3)

		sort.Slice(o.Packages[0].Items, func(i, j int) bool {
			return o.Packages[0].Items[i].ProductPid < o.Packages[0].Items[j].ProductPid
		})

		assert.Equal(t, 234, o.Packages[0].Items[0].ProductPid)

		assert.Equal(t, 298, o.Packages[0].Items[1].ProductPid)

		assert.Equal(t, 938, o.Packages[0].Items[2].ProductPid)
		assert.Len(t, o.Packages[0].Items[2].Skus, 1)
		assert.Len(t, o.Packages[0].Items[2].Skus[0].Codes, 1)
		assert.Equal(t, "18100488", o.Packages[0].Items[2].Skus[0].Codes[0])
		assert.Equal(t, 1, o.Packages[0].Items[2].Skus[0].Quantity)
	})
}

func TestSelectPrintedCRMKind(t *testing.T) {
	client = clientMock()

	printedCRMs, _ := fetchPrintedCRMs()

	o := &model.Order{
		State: "TX",
		Brand: "PAT",
		Items: []*model.Item{
			{
				ProductPid:   999,
				ProductBrand: "PAT",
			},
		},
		Packages: []*model.Package{
			{
				ID: 1,
				Items: []*model.Item{
					{
						ProductPid:   999,
						ProductBrand: "PAT",
					},
				},
			},
		},
	}
	pkgIdx := len(o.Packages) - 1
	t.Run("default catalogue", func(t *testing.T) {
		printedCRM := selectPrintedCRMKind(o, pkgIdx, model.PrintedCRMKindCatalogue, printedCRMs)
		assert.Equal(t, 939, printedCRM.ProductPid)
		assert.Equal(t, "PAT", printedCRM.ProductBrand)
	})

	t.Run("default welcome letter", func(t *testing.T) {
		printedCRM := selectPrintedCRMKind(o, pkgIdx, model.PrintedCRMKindWelcomeLetter, printedCRMs)
		assert.Equal(t, 938, printedCRM.ProductPid)
		assert.Equal(t, "PAT", printedCRM.ProductBrand)
	})

	t.Run("specific state", func(t *testing.T) {
		o.State = "CA"

		t.Run("catalogue", func(t *testing.T) {
			printedCRM := selectPrintedCRMKind(o, pkgIdx, model.PrintedCRMKindCatalogue, printedCRMs)
			assert.Equal(t, 940, printedCRM.ProductPid)
			assert.Equal(t, "PAT", printedCRM.ProductBrand)
		})

		t.Run("welcome letter", func(t *testing.T) {
			printedCRM := selectPrintedCRMKind(o, pkgIdx, model.PrintedCRMKindWelcomeLetter, printedCRMs)
			assert.Equal(t, 938, printedCRM.ProductPid)
			assert.Equal(t, "PAT", printedCRM.ProductBrand)
		})
	})
}

// localRoundTripper is an http.RoundTripper that executes HTTP transactions
// by using handler directly, instead of going over an HTTP connection.
type localRoundTripper struct {
	handler http.Handler
}

func (l localRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	w := httptest.NewRecorder()
	l.handler.ServeHTTP(w, req)
	return w.Result(), nil
}

func mustWrite(w io.Writer, s string) {
	_, err := io.WriteString(w, s)
	if err != nil {
		panic(err)
	}
}

func clientMock() graphclient.FHGraphql {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		body, _ := ioutil.ReadAll(req.Body)
		w.Header().Set("Content-Type", "application/json")
		if strings.Contains(string(body), "printedCRMs{") {
			writeCatalogResponse(w)
		} else {
			if strings.Contains(string(body), "createItem") {
				writeProductCreatedResponse(w)
			}
		}
	})

	return graphclient.NewFHGraphqlClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}},
	)
}

func writeProductCreatedResponse(w http.ResponseWriter) {
	mustWrite(w, `
		{
			"data": {
				"createItem": {
					"id": 123
				}
			}
		}
	`)
}

func writeCatalogResponse(w http.ResponseWriter) {
	mustWrite(w, `{
		"data": {
			"printedCRMs": [
				{
					"productPid": 938,
					"productBrand": "PAT",
					"kind": "WELCOME_LETTER",
					"states": []
				},
				{
					"productPid": 939,
					"productBrand": "PAT",
					"kind": "CATALOGUE",
					"states": []
				},
				{
					"productPid": 940,
					"productBrand": "PAT",
					"kind": "CATALOGUE",
					"states": [
						"CA"
					]
				},
				{
					"productPid": 646,
					"productBrand": "PHA",
					"kind": "WELCOME_LETTER",
					"states": []
				},
				{
					"productPid": 647,
					"productBrand": "PHA",
					"kind": "CATALOGUE",
					"states": []
				},
				{
					"productPid": 648,
					"productBrand": "PHA",
					"kind": "CATALOGUE",
					"states": [
						"CA"
					]
				}
			]
		}
	}`)
}

func mockGraphqlClient(response string) (c *graphql.Client) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_, err := w.Write([]byte(response))
		if err != nil {
			panic(err)
		}
	})

	c = graphql.NewClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}},
	)
	return
}
