package main

import (
	"context"
	"errors"
	"os"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
	"golang.org/x/sync/errgroup"
)

var log *logger.Log
var client graphclient.FHGraphql

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}
	client = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)
}

func getUPCForPrintedCRM(printedCrm model.PrintedCrm) string {
	switch printedCrm.ProductPid {
	case 938:
		return "18100488"
	case 939:
		return "810026143465"
	case 940:
		return "810026143472"
	case 646:
		return "18101023"
	case 647:
		return "810026150982"
	case 648:
		return "810026150999"
	default:
		return ""
	}
}

func fetchPrintedCRMs() (printedCRMs []model.PrintedCrm, err error) {
	type query struct {
		PrintedCRMs []model.PrintedCrm `graphql:"printedCRMs"`
	}

	var q query

	if err := client.Client.Query(context.Background(), &q, nil); err != nil {
		return nil, err
	}

	for _, printedCRM := range q.PrintedCRMs {
		var states []string
		for _, state := range printedCRM.States {
			states = append(states, string(state))
		}

		printedCRMs = append(printedCRMs, model.PrintedCrm{
			ProductPid:   printedCRM.ProductPid,
			ProductBrand: printedCRM.ProductBrand,
			Kind:         model.PrintedCRMKind(printedCRM.Kind),
			States:       states,
		})
	}

	return
}

func selectPrintedCRM(order *model.Order, pkgIdx int, all []model.PrintedCrm) (
	printedCRMs map[model.PrintedCRMKind]*model.PrintedCrm,
	err error,
) {
	printedCRMs = make(map[model.PrintedCRMKind]*model.PrintedCrm, 2)

	catalogue := selectPrintedCRMKind(order, pkgIdx, model.PrintedCRMKindCatalogue, all)
	if catalogue == nil {
		log.Logger.Printf("Failed to find catalogue for order %d", order.ID)
	} else {
		printedCRMs[model.PrintedCRMKindCatalogue] = catalogue
	}

	welcomeLetter := selectPrintedCRMKind(order, pkgIdx, model.PrintedCRMKindWelcomeLetter, all)
	if welcomeLetter == nil {
		log.Logger.Printf("Failed to find welcome letter for order %d", order.ID)
	} else {
		printedCRMs[model.PrintedCRMKindWelcomeLetter] = welcomeLetter
	}

	return
}

func checkNoCatalogPids(items []*model.Item) bool {
	var noCatalogRecords = map[string]map[int]int{
		"PAT": {
			17:  17,
			146: 146,
			234: 234,
			297: 297,
			298: 298,
			300: 300,
			315: 315,
		},
		"PHA": {
			99:  99,
			428: 428,
			272: 272,
			313: 313,
		},
	}

	for i := range items {
		if noCatalogPIDs, brandOk := noCatalogRecords[items[i].ProductBrand]; brandOk {
			if _, ok := noCatalogPIDs[items[i].ProductPid]; !ok {
				return false
			}
		} else {
			return false
		}
	}
	return true
}

func checkNoWelcomeLetterPids(items []*model.Item) bool {
	var noWelcomeLetterRecords = map[string]map[int]int{
		"PAT": {
			146: 146,
			234: 234,
		},
	}

	for i := range items {
		// If any item didn´t match we must skip and send Catalog send higher priority than exclusion when items > 1 (FH-927)
		if noLetterPIDs, brandOk := noWelcomeLetterRecords[items[i].ProductBrand]; brandOk {
			if _, ok := noLetterPIDs[items[i].ProductPid]; !ok {
				return false
			}
		} else {
			return false
		}
	}
	return true
}

func selectPrintedCRMKind(
	order *model.Order,
	pkgIdx int,
	kind model.PrintedCRMKind,
	printedCRMs []model.PrintedCrm,
) *model.PrintedCrm {
	var defaultPrintedCRM *model.PrintedCrm

	if pkgIdx > len(order.Packages) {
		return defaultPrintedCRM
	}

	for i := range printedCRMs {
		if kind == model.PrintedCRMKindCatalogue && checkNoCatalogPids(order.Packages[pkgIdx].Items) {
			continue
		}

		if kind == model.PrintedCRMKindWelcomeLetter && checkNoWelcomeLetterPids(order.Packages[pkgIdx].Items) {
			continue
		}

		if printedCRMs[i].ProductBrand != string(order.Brand) {
			continue
		}

		if printedCRMs[i].Kind != kind {
			continue
		}

		if len(printedCRMs[i].States) == 0 {
			defaultPrintedCRM = &printedCRMs[i]
			continue
		}

		for _, state := range printedCRMs[i].States {
			if state == order.State {
				return &printedCRMs[i]
			}
		}
	}

	return defaultPrintedCRM
}

func containsUPC(items []*model.Item, upc string) bool {
	for _, item := range items {
		for _, sku := range item.Skus {
			for _, code := range sku.Codes {
				if code == upc {
					return true
				}
			}
		}
	}

	return false
}

func addPrintedCRMs(order *model.Order) error {
	all, err := fetchPrintedCRMs()
	if err != nil {
		return err
	}

	if len(all) == 0 {
		return errors.New("graphql: printedCRMs records not found")
	}

	eg := new(errgroup.Group)

	for idx := range order.Packages {
		i := idx
		eg.Go(func() error {
			return addPackagePrintedCRMs(order, i, all)
		})
	}

	if err := eg.Wait(); err != nil {
		return err
	}

	return nil
}

func addPackagePrintedCRMs(order *model.Order, idx int, all []model.PrintedCrm) error {
	items, err := createPrintedCRMItems(order, idx, all)
	if err != nil {
		return err
	}

	for _, item := range items {
		if err := client.CreateItem(item); err != nil {
			return err
		}

		order.Packages[idx].Items = append(order.Packages[idx].Items, item)
	}
	return nil
}

func createPrintedCRMItems(order *model.Order, idx int, all []model.PrintedCrm) ([]*model.Item, error) {
	printedCRMs, err := selectPrintedCRM(order, idx, all)
	if err != nil {
		return nil, err
	}
	var items []*model.Item

	for _, printedCRM := range printedCRMs {
		upc := getUPCForPrintedCRM(*printedCRM)
		productPid := printedCRM.ProductPid
		productBrand := printedCRM.ProductBrand

		if printedCRM.Kind == model.PrintedCRMKindWelcomeLetter && containsUPC(order.Packages[idx].Items, "810026140044") {
			productPid = 941
			productBrand = "PAT"
			upc = "18100884"
		}

		if containsUPC(order.Packages[idx].Items, upc) {
			continue
		}

		var skus []*model.Skus
		skus = append(skus, &model.Skus{
			Codes:    []string{upc},
			Quantity: 1,
		})

		item := &model.Item{
			OrderID:      order.ID,
			PackageID:    &order.Packages[idx].ID,
			ProductPid:   productPid,
			ProductBrand: productBrand,
			Skus:         skus,
			Quantity:     1,
		}

		items = append(items, item)

	}
	return items, nil
}

func handler(ctx context.Context, order model.Order) (model.Order, error) {
	var err error
	salesChannelOrderID := int64(order.SalesChannelOrderID)
	log, err = logger.NewOrderLog(&order.ID, &salesChannelOrderID, (*string)(&order.SalesChannel), (*string)(&order.Brand))
	if err != nil {
		return model.Order{}, err
	}
	log.Logger.Info("20-add-printed-crm - start")

	err = addPrintedCRMs(&order)

	return order, err
}

func main() {
	lambda.Start(handler)
}
