package main

import (
	"context"
	"encoding/json"
	"os"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
)

// inputCatchAWS ...
type inputCatchAWS struct {
	OrderID      int
	ErrorMessage string
}

var log *logger.Log
var client graphclient.FHGraphql

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}
	client = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)

}

func main() {
	lambda.Start(handler)
}

func handler(ctx context.Context, input *inputCatchAWS) (err error) {
	log, err = logger.NewOrderLog(&input.OrderID, nil, nil, nil)
	if err != nil {
		return err
	}
	log.LogObject(input, "00-handle-error - start")

	var orderUpdate model.Order
	if orderUpdate, err = client.FindOrderByID(input.OrderID); err != nil {
		log.Logger.Printf("%+v", err.Error())
		return err
	}

	orderUpdate.Status = model.OrderStatusError
	orderUpdate.Error = &input.ErrorMessage
	log.Logger.Printf("%+v", *orderUpdate.Error)
	log.Logger.Printf("%+v", orderUpdate)
	if err = client.UpdateOrder(&orderUpdate); err != nil {
		log.Logger.Printf("%+v", err.Error())
		return err
	}

	return nil
}

// UnmarshalJSON ..
func (o *inputCatchAWS) UnmarshalJSON(data []byte) error {

	log.Logger.Printf("%+v", string(data))
	var v map[string]interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		log.Logger.Printf("%+v", err.Error())
		return err
	}

	o.OrderID = int(v["id"].(float64))

	var byteError []byte
	byteError, _ = json.Marshal(v["error"])

	type errorState struct {
		Cause string
		Error string
	}
	var errorMessages errorState
	if err := json.Unmarshal(byteError, &errorMessages); err != nil {
		log.Logger.Printf("%+v", err.Error())
		return err
	}

	type ErrorCause struct {
		ErrorMessage string `json:"errorMessage"`
		ErrorType    string `json:"errorType,omitempty"`
	}
	var errCause ErrorCause
	if err := json.Unmarshal([]byte(errorMessages.Cause), &errCause); err != nil {
		errCause.ErrorMessage = errorMessages.Cause
	}

	o.ErrorMessage = errCause.ErrorMessage

	return nil
}
