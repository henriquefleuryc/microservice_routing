package main

import (
	"encoding/json"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

func init() {
	godotenv.Load("./../../.env")
}

func TestOrder_unmarshalJSON(t *testing.T) {
	var resp inputCatchAWS
	var input = `{
		"id": 162,
		"parentId": null,
		"brand": "PHA",
		"items": [
		  {
			"id": 534,
			"orderId": 162,
			"packageId": null,
			"productPid": 195,
			"productBrand": "PHA",
			"originalPid": 1,
			"signatureRequired": false,
			"quantity": 1,
			"weight": 0,
			"skus": null,
			"trackingNumber": null,
			"createdAt": "2021-01-14T19:04:08Z",
			"updatedAt": "2021-01-14T19:04:08Z"
		  },
		  {
			"id": 537,
			"orderId": 162,
			"packageId": null,
			"productPid": 199,
			"productBrand": "PHA",
			"originalPid": 2,
			"signatureRequired": false,
			"quantity": 1,
			"weight": 0,
			"skus": null,
			"trackingNumber": null,
			"createdAt": "2021-01-14T19:04:08Z",
			"updatedAt": "2021-01-14T19:04:08Z"
		  }
		],
		"packages": [],
		"children": [],
		"firstname": "John",
		"lastname": "Test",
		"email": "test130@client.com.br",
		"phone": "",
		"street": "noship",
		"city": "Laredo",
		"zip": "78041",
		"state": "TX",
		"country": "US",
		"salesChannel": "SHOPIFY",
		"salesChannelOrderId": 310255744331,
		"status": "BUNDLED",
		"upgradedById": null,
		"upgradedAt": null,
		"paused": false,
		"pausedAt": null,
		"pauseReason": null,
		"resumedAt": null,
		"taskToken": null,
		"error": {
		  "Error": "errorString",
		  "Cause": "{\"errorMessage\":\"Error searching Rules Item: \\u0026{ID:534 OrderID:162 PackageID:\\u003cnil\\u003e ProductPid:195 ProductBrand:PHA OriginalPid:1 SignatureRequired:false Quantity:1 Weight:0 Skus:[] TrackingNumber:\\u003cnil\\u003e CreatedAt:2021-01-14T19:04:08Z UpdatedAt:2021-01-14T19:04:08Z} Error: record not found\",\"errorType\":\"errorString\"}"
		},
		"fieldsUpdated": null,
		"createdAt": "0001-01-01T00:00:00Z",
		"updatedAt": "2021-01-14T19:04:08Z"
	  }`

	type args struct {
		data []byte
	}
	tests := []struct {
		name    string
		to      *inputCatchAWS
		args    args
		wantErr bool
	}{
		{
			"Custom unmarshalJSON Order",
			&resp,
			args{
				[]byte(input),
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.to.UnmarshalJSON(tt.args.data); (err != nil) != tt.wantErr {
				t.Errorf("Order.unmarshalJSON() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, 162, tt.to.OrderID)
		})
	}
}

func TestUnmarshalJSONShouldDeserializeErrorPayload(t *testing.T) {
	// Arrange
	assert := assert.New(t)
	payload := `{"id":31278,"parentId":null,"brand":"PAT","items":[{"id":90873,"orderId":31278,"packageId":null,"productPid":385,"productBrand":"PAT","originalPid":385,"signatureRequired":true,"quantity":1,"weight":29.6,"skus":[{"codes":["810026140518"],"quantity":2},{"codes":["810026140532"],"quantity":1}],"trackingNumber":null,"createdAt":"2021-03-25T16:17:08Z","updatedAt":"2021-03-25T16:17:08Z"}],"packages":[{"id":27910,"orderId":31278,"vendorId":2,"packageVendorId":null,"items":[{"id":90873,"orderId":31278,"packageId":27910,"productPid":385,"productBrand":"PAT","originalPid":385,"signatureRequired":true,"quantity":1,"weight":29.6,"skus":[{"codes":["810026140518"],"quantity":2},{"codes":["810026140532"],"quantity":1}],"trackingNumber":null,"createdAt":"2021-03-25T16:17:08Z","updatedAt":"2021-03-25T16:17:08Z"},{"id":93456,"orderId":31278,"packageId":27910,"productPid":939,"productBrand":"PAT","originalPid":0,"signatureRequired":false,"quantity":1,"weight":0,"skus":[{"codes":["810026143465"],"quantity":1}],"trackingNumber":null,"createdAt":"2021-03-25T16:17:09Z","updatedAt":"2021-03-25T16:17:09Z"},{"id":93459,"orderId":31278,"packageId":27910,"productPid":938,"productBrand":"PAT","originalPid":0,"signatureRequired":false,"quantity":1,"weight":0,"skus":[{"codes":["18100488"],"quantity":1}],"trackingNumber":null,"createdAt":"2021-03-25T16:17:09Z","updatedAt":"2021-03-25T16:17:09Z"}],"weight":29.6,"carrier":"","status":"CREATED","statusDescription":null,"estimatedDeliveryDate":null,"paused":null,"error":null,"shippingCarrier":"UPS","shippingMethod":"Residential","shippingCode":"UPSRTBSR","createdAt":"2021-03-25T16:17:08Z","updatedAt":"2021-03-25T16:17:08Z","is65prod":null,"information65propId":null,"signatureRequired":true,"vendorRequestPayload":null,"vendorResponsePayload":null}],"children":[],"firstname":"Fritz","lastname":"test","email":"a@risusIn.net","phone":"(251) 675-4044","street":"noship","city":"New Haven","zip":"44776","state":"CT","country":"US","salesChannel":"NSP","salesChannelOrderId":5654676,"status":"BUNDLED","upgradedById":null,"upgradedAt":null,"paused":false,"pausedAt":null,"pauseReason":null,"resumedAt":null,"taskToken":null,"error":{"Error":"Lambda.Unknown","Cause":"The cause could not be determined because Lambda did not return an error type. Returned payload: {\"errorMessage\":\"2021-03-25T16:22:20.853Z 20286af2-5ca5-47bc-9294-05068f8386e5 Task timed out after 60.03 seconds\"}"},"fieldsUpdated":null,"createdAt":"0001-01-01T00:00:00Z","updatedAt":"2021-03-25T16:17:08Z"}`

	res := &inputCatchAWS{}

	// Act

	err := json.Unmarshal([]byte(payload), res)

	// Assert
	assert.NoError(err)
}
