package main

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"

	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
)

func init() {
	godotenv.Load("./../../.env")
}

func TestPersistOrder(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		mustWrite(w, `{
			"data": {
				"updateOrder": {
					"id": 297,
					"parentId": null,
					"brand": "PAT",
					"items": [
						{
							"id": 587,
							"orderId": 297,
							"packageId": null,
							"productPid": 610,
							"productBrand": "PAT",
							"originalPid": 610,
							"signatureRequired": false,
							"quantity": 1,
							"weight": 15.9,
							"skus": [
								{
									"codes": [
										"810026141768"
									],
									"quantity": 1
								}
							],
							"trackingNumber": null,
							"createdAt": "2021-02-27T01:25:53Z",
							"updatedAt": "2021-02-27T01:25:53Z"
						}
					],
					"packages": [
						{
							"id": 152,
							"orderId": 297,
							"vendorId": 4,
							"packageVendorId": null,
							"items": [
								{
									"id": 587,
									"orderId": 297,
									"packageId": 152,
									"productPid": 610,
									"productBrand": "PAT",
									"originalPid": 610,
									"signatureRequired": false,
									"quantity": 1,
									"weight": 15.9,
									"trackingNumber": null,
									"createdAt": "2021-02-27T01:25:53Z",
									"updatedAt": "2021-02-27T01:25:53Z"
								},
								{
									"id": 588,
									"orderId": 297,
									"packageId": 152,
									"productPid": 940,
									"productBrand": "PAT",
									"originalPid": 0,
									"signatureRequired": false,
									"quantity": 1,
									"weight": 0,
									"trackingNumber": null,
									"createdAt": "2021-02-27T01:25:53Z",
									"updatedAt": "2021-02-27T01:25:53Z"
								},
								{
									"id": 589,
									"orderId": 297,
									"packageId": 152,
									"productPid": 938,
									"productBrand": "PAT",
									"originalPid": 0,
									"signatureRequired": false,
									"quantity": 1,
									"weight": 0,
									"trackingNumber": null,
									"createdAt": "2021-02-27T01:25:53Z",
									"updatedAt": "2021-02-27T01:25:53Z"
								}
							],
							"weight": 15.9,
							"carrier": "",
							"status": "CREATED",
							"statusDescription": null,
							"estimatedDeliveryDate": null,
							"paused": null,
							"error": null,
							"shippingCarrier": "FEDEX",
							"shippingMethod": "Ground",
							"shippingCode": "FGNDRTB",
							"createdAt": "2021-02-27T01:25:53Z",
							"updatedAt": "2021-02-27T01:25:53Z",
							"is65prod": true,
							"information65propId": 19,
							"signatureRequired": false,
							"vendorRequestPayload": null,
							"vendorResponsePayload": null
						}
					],
					"children": [],
					"firstname": "Marcio",
					"lastname": "Oliveira",
					"email": "marcio.oliveira@client.com",
					"phone": "",
					"street": "noship",
					"city": "Los Angeles",
					"zip": "90037",
					"state": "CA",
					"country": "US",
					"salesChannel": "SHOPIFY",
					"salesChannelOrderId": 4078433528989,
					"status": "BUNDLED",
					"upgradedById": null,
					"upgradedAt": null,
					"paused": false,
					"pausedAt": null,
					"pauseReason": null,
					"resumedAt": null,
					"taskToken": null,
					"error": null,
					"fieldsUpdated": null,
					"createdAt": "0001-01-01T00:00:00Z",
					"updatedAt": "2021-02-27T01:25:53Z"
				}
			}
		}`)
	})

	client = graphclient.NewFHGraphqlClient(
		"/query",
		&http.Client{Transport: localRoundTripper{handler: mux}},
	)

	orderJSON := `{
		"id": 297,
		"parentId": null,
		"brand": "PAT",
		"items": [
		  {
			"id": 587,
			"orderId": 297,
			"packageId": null,
			"productPid": 610,
			"productBrand": "PAT",
			"originalPid": 610,
			"signatureRequired": false,
			"quantity": 1,
			"weight": 15.9,
			"skus": [
			  {
				"codes": [
				  "810026141768"
				],
				"quantity": 1
			  }
			],
			"trackingNumber": null,
			"createdAt": "2021-02-13T15:36:16Z",
			"updatedAt": "2021-02-13T15:36:16Z"
		  }
		],
		"packages": [
		  {
			"id": 152,
			"orderId": 297,
			"vendorId": 4,
			"packageVendorId": null,
			"items": [
			  {
				"id": 587,
				"orderId": 297,
				"packageId": 152,
				"productPid": 610,
				"productBrand": "PAT",
				"originalPid": 610,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 15.9,
				"skus": [
				  {
					"codes": [
					  "810026141768"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-02-13T16:54:47Z",
				"updatedAt": "2021-02-13T16:54:47Z"
			  },
			  {
				"id": 588,
				"orderId": 297,
				"packageId": 152,
				"productPid": 940,
				"productBrand": "PAT",
				"originalPid": 0,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0,
				"skus": [
				  {
					"codes": [
					  "810026143472"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-02-13T16:54:48Z",
				"updatedAt": "2021-02-13T16:54:48Z"
			  },
			  {
				"id": 589,
				"orderId": 297,
				"packageId": 152,
				"productPid": 938,
				"productBrand": "PAT",
				"originalPid": 0,
				"signatureRequired": false,
				"quantity": 1,
				"weight": 0,
				"skus": [
				  {
					"codes": [
					  "18100488"
					],
					"quantity": 1
				  }
				],
				"trackingNumber": null,
				"createdAt": "2021-02-13T16:54:48Z",
				"updatedAt": "2021-02-13T16:54:48Z"
			  }
			],
			"weight": 15.9,
			"carrier": "",
			"status": "CREATED",
			"statusDescription": null,
			"estimatedDeliveryDate": null,
			"paused": null,
			"error": null,
			"shippingCarrier": "FEDEX",
			"shippingMethod": "Ground",
			"shippingCode": "FGNDRTB",
			"createdAt": "2021-02-13T16:54:47Z",
			"updatedAt": "2021-02-13T16:54:47Z",
			"is65prod": true,
			"information65propId": 19,
			"signatureRequired": false,
			"vendorRequestPayload": null,
			"vendorResponsePayload": null
		  }
		],
		"children": [],
		"firstname": "Marcio",
		"lastname": "Oliveira",
		"email": "marcio.oliveira@client.com",
		"phone": "",
		"street": "noship",
		"city": "Los Angeles",
		"zip": "90037",
		"state": "CA",
		"country": "US",
		"salesChannel": "SHOPIFY",
		"salesChannelOrderId": 4078433528989,
		"status": "BUNDLED",
		"upgradedById": null,
		"upgradedAt": null,
		"paused": false,
		"pausedAt": null,
		"pauseReason": null,
		"resumedAt": null,
		"taskToken": null,
		"error": null,
		"fieldsUpdated": null,
		"createdAt": "0001-01-01T00:00:00Z",
		"updatedAt": "2021-02-13T15:36:16Z"
	  }`

	order := &model.Order{}
	json.Unmarshal([]byte(orderJSON), &order)

	err := PersistOrder(*order)
	assert.Nil(t, err)
	assert.NotNil(t, order)
	assert.NotNil(t, order.Packages)
	assert.Equal(t, 1, len(order.Packages))
	assert.NotNil(t, order.Packages[0].Is65prod)
	assert.NotNil(t, order.Packages[0].Information65propID)
}

// localRoundTripper is an http.RoundTripper that executes HTTP transactions
// by using handler directly, instead of going over an HTTP connection.
type localRoundTripper struct {
	handler http.Handler
}

func (l localRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	w := httptest.NewRecorder()
	l.handler.ServeHTTP(w, req)
	return w.Result(), nil
}

func mustWrite(w io.Writer, s string) {
	_, err := io.WriteString(w, s)
	if err != nil {
		panic(err)
	}
}
