package main

import (
	"context"
	"os"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
)

var log *logger.Log
var client graphclient.FHGraphql

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}
	client = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)
}

func PersistOrder(order model.Order) error {
	return client.UpdateOrder(&order)
}

func handler(ctx context.Context, order model.Order) (model.Order, error) {
	var err error
	salesChannelOrderID := int64(order.SalesChannelOrderID)
	log, err = logger.NewOrderLog(&order.ID, &salesChannelOrderID, (*string)(&order.SalesChannel), (*string)(&order.Brand))
	if err != nil {
		return model.Order{}, err
	}
	log.Logger.Info("50-persist-order - start")

	err = PersistOrder(order)
	return order, err
}

func main() {
	lambda.Start(handler)
}
