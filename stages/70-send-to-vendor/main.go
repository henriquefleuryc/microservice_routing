package main

import (
	"context"
	"os"
	"time"

	logger "github.com/client/fh-logger"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
	vendors "github.com/client/fh-vendors"
)

var log *logger.Log
var client graphclient.FHGraphql

const timeout = 60 * time.Second

func init() {
	log = logger.NewLog()
	audience := os.Getenv("GOOGLE_OAUTH_AUDIENCE")
	credentialsJSON := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	httpClient, err := graphclient.NewIDTokenHTTPClient(audience, []byte(credentialsJSON))
	if err != nil {
		log.Logger.Print(err)
	}
	client = graphclient.NewFHGraphqlClient(os.Getenv("GRAPHQL_API_URL"), httpClient)
}

func getOrder(orderID *int) *model.Order {
	var query = model.OrderFilterInput{
		ID: orderID,
	}
	orders, err := client.GetOrders(query)
	if err == nil && len(orders) == 1 {
		return orders[0]
	}
	return nil
}

func handler(ctx context.Context, input model.Order) (model.Order, error) {
	var err error
	salesChannelOrderID := int64(input.SalesChannelOrderID)
	log, err = logger.NewOrderLog(&input.ID, &salesChannelOrderID, (*string)(&input.SalesChannel), (*string)(&input.Brand))
	if err != nil {
		return model.Order{}, err
	}
	log.Logger.Info("70-send-to-vendor - start")

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	errors := make(chan error, 1)

	o := getOrder(&input.ID)
	if o != nil {
		errors <- vendors.CreateVendorPackages(o)
	} else {
		errors <- vendors.CreateVendorPackages(&input)
	}

	select {
	case err := <-errors:
		o = getOrder(&input.ID)
		if o != nil {
			return *o, err
		}
	case <-ctx.Done():
		err = ctx.Err()
		return input, err
	}

	return input, err
}

func main() {
	lambda.Start(handler)
}
