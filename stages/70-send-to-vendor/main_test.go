package main

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/h2non/gock.v1"

	"github.com/client/fh-graphql/graph/model"
	"github.com/client/fh-graphql/graphclient"
)

const (
	fakeAPIURL     = "http://localhost:9999"
	fakeGraphQLURL = "http://localhost:9998"
	fakeAPIToken   = "1234-abcd-5678-efgh"
)

func init() {
	os.Setenv("ENLINX_PAT_APITOKEN", fakeAPIToken)
	os.Setenv("ENLINX_PAT_URL", fakeAPIURL)
	os.Setenv("ENLINX_PHA_APITOKEN", fakeAPIToken)
	os.Setenv("ENLINX_PHA_URL", fakeAPIURL)
	os.Setenv("GRAPHQL_API_URL", fakeAPIURL)
	os.Setenv("TRACKNUMBER_QUEUE_ARN", "arn:aws:sqs:us-east-1:188174211009:qa-fh-tracknumber-queue")
	os.Setenv("AWS_XRAY_CONTEXT_MISSING", "LOG_ERROR")
	os.Setenv("AWS_XRAY_SDK_DISABLED", "TRUE")
}

func TestSendToVendor(t *testing.T) {
	defer gock.Off()

	gock.New(fakeAPIURL).
		Get("/order/new").
		Reply(200).
		BodyString(`{"code": "200","message": "Backorder: Insufficient inventory to fulfil order","status": "success"}`)

	orderJSON := `{"id":318,"parentId":null,"brand":"PAT","items":[{"id":636,"orderId":318,"packageId":168,"productPid":326,"productBrand":"PAT","originalPid":326,"signatureRequired":false,"quantity":1,"weight":4.6,"skus":[{"codes":["810026140518"],"quantity":2}],"trackingNumber":null,"createdAt":"2021-02-15T19:23:45Z","updatedAt":"2021-02-15T19:28:51Z"},{"id":641,"orderId":318,"packageId":168,"productPid":939,"productBrand":"PAT","originalPid":0,"signatureRequired":false,"quantity":1,"weight":0,"skus":[{"codes":["810026143465"],"quantity":1}],"trackingNumber":null,"createdAt":"2021-02-15T19:28:48Z","updatedAt":"2021-02-15T19:28:51Z"},{"id":642,"orderId":318,"packageId":168,"productPid":938,"productBrand":"PAT","originalPid":0,"signatureRequired":false,"quantity":1,"weight":0,"skus":[{"codes":["18100488"],"quantity":1}],"trackingNumber":null,"createdAt":"2021-02-15T19:28:48Z","updatedAt":"2021-02-15T19:28:51Z"}],"packages":[{"id":168,"orderId":318,"vendorId":4,"packageVendorId":"PAT5493665-0","items":[{"id":636,"orderId":318,"packageId":168,"productPid":326,"productBrand":"PAT","originalPid":326,"signatureRequired":false,"quantity":1,"weight":4.6,"trackingNumber":null,"createdAt":"2021-02-15T19:23:45Z","updatedAt":"2021-02-15T19:28:51Z"},{"id":637,"orderId":319,"packageId":168,"productPid":92,"productBrand":"PAT","originalPid":92,"signatureRequired":false,"quantity":1,"weight":8,"trackingNumber":null,"createdAt":"2021-02-15T19:23:47Z","updatedAt":"2021-02-15T19:28:51Z"},{"id":638,"orderId":320,"packageId":168,"productPid":128,"productBrand":"PAT","originalPid":128,"signatureRequired":false,"quantity":1,"weight":5,"trackingNumber":null,"createdAt":"2021-02-15T19:23:51Z","updatedAt":"2021-02-15T19:28:51Z"},{"id":641,"orderId":318,"packageId":168,"productPid":939,"productBrand":"PAT","originalPid":0,"signatureRequired":false,"quantity":1,"weight":0,"trackingNumber":null,"createdAt":"2021-02-15T19:28:48Z","updatedAt":"2021-02-15T19:28:51Z"},{"id":642,"orderId":318,"packageId":168,"productPid":938,"productBrand":"PAT","originalPid":0,"signatureRequired":false,"quantity":1,"weight":0,"trackingNumber":null,"createdAt":"2021-02-15T19:28:48Z","updatedAt":"2021-02-15T19:28:51Z"}],"weight":17.6,"carrier":"","status":"INTEGRATED","statusDescription":null,"estimatedDeliveryDate":null,"paused":null,"error":null,"shippingCarrier":"FEDEX","shippingMethod":"Ground","shippingCode":"FGNDRTB","createdAt":"2021-02-15T19:28:47Z","updatedAt":"2021-02-15T19:28:51Z","is65prod":null,"information65propId":null,"signatureRequired":false,"vendorRequestPayload":"{\"id\":\"PAT5493665-0\",\"customer_name\":\"Ricardo Test\",\"shipping_first_name\":\"Ricardo\",\"shipping_last_name\":\"Test\",\"shipping_address_1\":\"noship\",\"shipping_address_2\":\"\",\"shipping_city\":\"Orlando\",\"shipping_state\":\"FL\",\"shipping_zip\":\"12345\",\"shipping_country\":\"US\",\"shipping_email\":\"StephTest1785417@4pat-fulfillment.com\",\"shipping_phone\":\"1111111111\",\"ship_by_date\":\"0001-01-01T00:00:00Z\",\"create_at\":\"0001-01-01T00:00:00Z\",\"shipping_carrier_code\":\"FGNDRTB\",\"shipping_account_no\":\"1831V0\",\"total_value\":0,\"order_notes\":\"318-92::-128::-326::-939::-938\",\"order_class_code\":\"OTEST\",\"line_items\":[{\"id\":\"637\",\"sku\":\"810026140525\",\"price\":0,\"quantity\":1,\"notes\":\"92\",\"upc\":\"\",\"ucc128\":\"\",\"skuDesc\":\"\",\"lot\":\"\",\"lotExpirationDate\":\"\",\"containerNumber\":\"\",\"containerWeight\":\"\",\"expectedPackagedAmount\":0,\"actualPackagedAmount\":0,\"externalLineId\":\"\",\"orderLineNumber\":0,\"quantityRemaining\":0},{\"id\":\"638\",\"sku\":\"810026140556\",\"price\":0,\"quantity\":1,\"notes\":\"128\",\"upc\":\"\",\"ucc128\":\"\",\"skuDesc\":\"\",\"lot\":\"\",\"lotExpirationDate\":\"\",\"containerNumber\":\"\",\"containerWeight\":\"\",\"expectedPackagedAmount\":0,\"actualPackagedAmount\":0,\"externalLineId\":\"\",\"orderLineNumber\":0,\"quantityRemaining\":0},{\"id\":\"636\",\"sku\":\"810026140518\",\"price\":0,\"quantity\":2,\"notes\":\"326\",\"upc\":\"\",\"ucc128\":\"\",\"skuDesc\":\"\",\"lot\":\"\",\"lotExpirationDate\":\"\",\"containerNumber\":\"\",\"containerWeight\":\"\",\"expectedPackagedAmount\":0,\"actualPackagedAmount\":0,\"externalLineId\":\"\",\"orderLineNumber\":0,\"quantityRemaining\":0},{\"id\":\"641\",\"sku\":\"810026143465\",\"price\":0,\"quantity\":1,\"notes\":\"939\",\"upc\":\"\",\"ucc128\":\"\",\"skuDesc\":\"\",\"lot\":\"\",\"lotExpirationDate\":\"\",\"containerNumber\":\"\",\"containerWeight\":\"\",\"expectedPackagedAmount\":0,\"actualPackagedAmount\":0,\"externalLineId\":\"\",\"orderLineNumber\":0,\"quantityRemaining\":0},{\"id\":\"642\",\"sku\":\"18100488\",\"price\":0,\"quantity\":1,\"notes\":\"938\",\"upc\":\"\",\"ucc128\":\"\",\"skuDesc\":\"\",\"lot\":\"\",\"lotExpirationDate\":\"\",\"containerNumber\":\"\",\"containerWeight\":\"\",\"expectedPackagedAmount\":0,\"actualPackagedAmount\":0,\"externalLineId\":\"\",\"orderLineNumber\":0,\"quantityRemaining\":0}]}","vendorResponsePayload":"{\n\t\"code\": \"200\",\n\t\"message\": \"Backorder: Insufficient inventory to fulfil order\",\n\t\"status\": \"success\"\n}"}],"children":[{"id":319,"parentId":318,"brand":"PAT","items":[{"id":637,"orderId":319,"packageId":168,"productPid":92,"productBrand":"PAT","originalPid":92,"signatureRequired":false,"quantity":1,"weight":8,"trackingNumber":null,"createdAt":"2021-02-15T19:23:47Z","updatedAt":"2021-02-15T19:28:51Z"}],"firstname":"Ricardo","lastname":"Test","email":"RicardoTest1613416846@test.com","phone":"7891234560","street":"noship","city":"Orlando","zip":"12345","state":"FL","country":"US","salesChannel":"PORTAL","salesChannelOrderId":5493667,"status":"BUNDLED","upgradedById":null,"upgradedAt":null,"taskToken":null,"error":null,"fieldsUpdated":null,"createdAt":"2021-02-15T19:23:47Z","updatedAt":"2021-02-15T19:28:46Z"},{"id":320,"parentId":318,"brand":"PAT","items":[{"id":638,"orderId":320,"packageId":168,"productPid":128,"productBrand":"PAT","originalPid":128,"signatureRequired":false,"quantity":1,"weight":5,"trackingNumber":null,"createdAt":"2021-02-15T19:23:51Z","updatedAt":"2021-02-15T19:28:51Z"}],"firstname":"Ricardo","lastname":"Test","email":"RicardoTest1613416846@test.com","phone":"7891234560","street":"noship","city":"Orlando","zip":"12345","state":"FL","country":"US","salesChannel":"PORTAL","salesChannelOrderId":5493670,"status":"BUNDLED","upgradedById":null,"upgradedAt":null,"taskToken":null,"error":null,"fieldsUpdated":null,"createdAt":"2021-02-15T19:23:51Z","updatedAt":"2021-02-15T19:28:46Z"}],"firstname":"Ricardo","lastname":"Test","email":"RicardoTest1613416846@test.com","phone":"7891234560","street":"noship","city":"Orlando","zip":"12345","state":"FL","country":"US","salesChannel":"PORTAL","salesChannelOrderId":5493665,"status":"BUNDLED","upgradedById":null,"upgradedAt":null,"paused":false,"pausedAt":null,"pauseReason":null,"resumedAt":null,"taskToken":null,"error":null,"fieldsUpdated":null,"createdAt":"2021-02-15T19:23:45Z","updatedAt":"2021-02-15T19:28:48Z"}`

	mux := http.NewServeMux()
	mux.HandleFunc("/query", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		io.WriteString(w, `{"data":{"orders":[{"id":318,"parentId":null,"paused":false,"pausedAt":null,"pauseReason":null,"brand":"PAT","email":"RicardoTest1613416846@test.com","firstname":"Ricardo","lastname":"Test","country":"US","state":"FL","city":"Orlando","street":"noship","zip":"12345","phone":"7891234560","salesChannelOrderId":5493665,"salesChannel":"PORTAL","upgradedById":null,"status":"BUNDLED","items":[{"id":636,"packageId":168,"productPid":326,"productBrand":"PAT","originalPid":326,"signatureRequired":false,"quantity":1,"weight":4.6,"skus":[{"codes":["810026140518"],"quantity":2}],"trackingNumber":null,"createdAt":"2021-02-15T19:23:45Z","updatedAt":"2021-02-15T19:28:51Z"},{"id":641,"packageId":168,"productPid":939,"productBrand":"PAT","originalPid":0,"signatureRequired":false,"quantity":1,"weight":0,"skus":[{"codes":["810026143465"],"quantity":1}],"trackingNumber":null,"createdAt":"2021-02-15T19:28:48Z","updatedAt":"2021-02-15T19:28:51Z"},{"id":642,"packageId":168,"productPid":938,"productBrand":"PAT","originalPid":0,"signatureRequired":false,"quantity":1,"weight":0,"skus":[{"codes":["18100488"],"quantity":1}],"trackingNumber":null,"createdAt":"2021-02-15T19:28:48Z","updatedAt":"2021-02-15T19:28:51Z"}],"packages":[{"id":168,"orderId":318,"vendorId":4,"weight":17.6,"carrier":"","status":"INTEGRATED","statusDescription":null,"estimatedDeliveryDate":null,"shippingCarrier":"FEDEX","shippingMethod":"Ground","shippingCode":"FGNDRTB","is65prod":null}],"children":[{"id":319,"parentId":318,"firstname":"Ricardo"},{"id":320,"parentId":318,"firstname":"Ricardo"}]}]}}`)
	})

	client = graphclient.NewFHGraphqlClient("/query", &http.Client{Transport: localRoundTripper{handler: mux}})

	inputOrder := &model.Order{}
	json.Unmarshal([]byte(orderJSON), &inputOrder)

	o, err := handler(nil, *inputOrder)
	assert.Nil(t, err)

	assert.NotNil(t, o)
	assert.NotNil(t, o.Packages)
	assert.Equal(t, 1, len(o.Packages))
	assert.Equal(t, model.PackageStatusIntegrated, o.Packages[0].Status)
}

type localRoundTripper struct {
	handler http.Handler
}

func (rt localRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	w := httptest.NewRecorder()
	rt.handler.ServeHTTP(w, req)
	return w.Result(), nil
}
