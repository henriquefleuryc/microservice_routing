# fh-routing 

This repository contains the code for the state machine in charge of performing the routing of orders to the different vendors.

This is the second part of the order processing workflow, and is triggered by the last stage of the [`fh-ingestion` state machine.

## Folder structure

The following graph summarizes the structure of this repo and some information about the contents of every folder on it.

```
root <-- files for deployment on AWS using SAM CLI
└───stages   <-- folders with code for each state machine stage
    ├───00-handle-error
    ├───10-get-pim-data
    ├───10-routing
    ├───20-add-printed-crm
    ├───30-add-prop-65
    ├───40-select-carrier
    ├───50-persist-order
    ├───60-check-vendor-paused
    └───70-send-to-vendor
```

### Stages
Every stage on the state machine has the code stored on a folder, the equivalency of those is detailed below:
|Stage|Folder|Description|
|-|-|-|
|Get PIM Data|05-get-pim-data|Queries PIM to obtain relevant data from PIM for the processed order and it's childs|
|Routing|10-routing|Applies the routing rules to determine which vendor should ship the items|
|Add Printed CRM|20-add-printed-crm|Adds the printed catalog items to the order|
|Add Prop 65|30-add-prop-65|Add information to the products regarding the 65th proposition|
|Select Carrier|40-select-carrier|Determines the carrier to ship the packages|
|Persist Order|50-persist-order|Updates the order on the database with the added information from previous steps|
|Check Vendor Paused|60-check-vendor-paused|Checks if the vendor is paused, if so then changes the status of the package to `VENDOR_PAUSED`|
|Send To Vendor|70-send-to-vendor|Sends the package(s) to the vendors system for it's processing|
|Handle Error|00-handle-error|Gets the error on the order, changes its status to `ERROR` and exits gracefully from the statemachine|


## Running the tests

To run the tests you first need to spin up a `DynamoDb` docker container, to do this run the command `docker-compose build && docker-compose up`, this will setup the required instance and network.

To start the tests run the command `go test ./...`

## Dependencies

This repository has the following dependencies:

<table>
    <thead>
        <tr><th>Kind</th><th>Name</th><th>Associated Secrets</th>
    </thead>
    <tbody>
        <tr>
            <td>SNS Topic</td>
            <td>FHNotificationSNSTopic</td>
            <td>FH_NOTIFICATION_SNS_TOPIC</td>
        </tr>
        <tr>
            <td>Dynamo Table</td>
            <td>FHRoutingCarrierDistribution</td>
            <td>FHRoutingCarrierDistribution-${Environment}</td>
        </tr>
        <tr>
            <td rowspan=3>Library</td>
            <td>fh-graphql</td>
            <td></td>
        </tr>
        <tr>
            <td>fh-sns</td>
            <td></td>
        </tr>
        <tr>
            <td>fh-vendors</td>
            <td></td>
        </tr>
        <tr>
            <td rowspan=22>Env. Variable</td>
            <td>GRAPHQL_API_URL</td>
            <td>https://fh.${Environment}.client.net/query</td>
        </tr>
        <tr>
            <td>PIM_SERVICE_URL</td>
            <td>https://pim-fh.sb.client.net/query</td>
        </tr>
        <tr>
            <td>ENLINX_PAT_URL</td>
            <td>{{resolve:secretsmanager:fh:SecretString:enlinxPatUrl}}</td>
        </tr>
        <tr>
            <td>ENLINX_PAT_APITOKEN</td>
            <td>{{resolve:secretsmanager:fh:SecretString:enlinxPatApiToken}}</td>
        </tr>
        <tr>
            <td>ENLINX_PHA_URL</td>
            <td>{{resolve:secretsmanager:fh:SecretString:enlinxPhaUrl}}</td>
        </tr>
        <tr>
            <td>ENLINX_PHA_APITOKEN</td>
            <td>{{resolve:secretsmanager:fh:SecretString:enlinxPhaApiToken}}</td>
        </tr>
        <tr>
            <td>LION_PAT_URL</td>
            <td>{{resolve:secretsmanager:fh:SecretString:lionPatUrl}}</td>
        </tr>
        <tr>
            <td>LION_PAT_APITOKEN</td>
            <td>{{resolve:secretsmanager:fh:SecretString:lionPatApiToken}}</td>
        </tr>
        <tr>
            <td>LION_PHA_URL</td>
            <td>{{resolve:secretsmanager:fh:SecretString:lionPhaUrl}}</td>
        </tr>
        <tr>
            <td>LION_PHA_APITOKEN</td>
            <td>{{resolve:secretsmanager:fh:SecretString:lionPhaApiToken}}</td>
        </tr>
        <tr>
            <td>VISIBLE_PAT_URL</td>
            <td>{{resolve:secretsmanager:fh:SecretString:visiblePatUrl}}</td>
        </tr>
        <tr>
            <td>VISIBLE_PAT_USERNAME</td>
            <td>{{resolve:secretsmanager:fh:SecretString:visiblePatUsername}}</td>
        </tr>
        <tr>
            <td>VISIBLE_PAT_PASSWORD</td>
            <td>{{resolve:secretsmanager:fh:SecretString:visiblePatPassword}}</td>
        </tr>
        <tr>
            <td>VISIBLE_PHA_URL</td>
            <td>{{resolve:secretsmanager:fh:SecretString:visiblePhaUrl}}</td>
        </tr>
        <tr>
            <td>VISIBLE_PHA_USERNAME</td>
            <td>{{resolve:secretsmanager:fh:SecretString:visiblePhaUsername}}</td>
        </tr>
        <tr>
            <td>VISIBLE_PHA_PASSWORD</td>
            <td>{{resolve:secretsmanager:fh:SecretString:visiblePhaPassword}}</td>
        </tr>
        <tr>
            <td>ESM_PAT_URL</td>
            <td>{{resolve:secretsmanager:fh:SecretString:esmPatUrl}}</td>
        </tr>
        <tr>
            <td>ESM_PAT_APITOKEN</td>
            <td>{{resolve:secretsmanager:fh:SecretString:esmPatApiToken}}</td>
        </tr>
        <tr>
            <td>ESM_PHA_URL</td>
            <td>{{resolve:secretsmanager:fh:SecretString:esmPhaUrl}}</td>
        </tr>
        <tr>
            <td>ESM_PHA_APITOKEN</td>
            <td>{{resolve:secretsmanager:fh:SecretString:esmPhaApiToken}}</td>
        </tr>
        <tr>
            <td>GOOGLE_OAUTH_AUDIENCE</td>
            <td>{{resolve:secretsmanager:${Environment}/fh/routing:SecretString:GOOGLE_OAUTH_AUDIENCE}}</td>
        </tr>
        <tr>
            <td>GOOGLE_CREDENTIALS_JSON</td>
            <td>{{resolve:secretsmanager:${Environment}/fh/routing:SecretString:GOOGLE_CREDENTIALS_JSON}}</td>
        </tr>
    </tbody>
</table>